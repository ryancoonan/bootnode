# region Imports
from __future__ import absolute_import
# standard lib
from enum import IntEnum
# endregion


# region Public
class BootnodeError(IntEnum):
    # general 1 - 99
    unknown = 1

    # request validation 100 - 199
    missing_required_parameter = 100
    invalid_password_length = 101
    method_not_supported = 102
    invalid_email = 103
    email_too_long = 104
    invalid_node_name = 105
    user_is_locked_out = 106

    # user permissions 200 - 299
    wrong_node_owner = 200
    wrong_user = 201

    # resource states 300 - 399
    node_not_powered_on = 300
    ip_allocation_failed = 301

    # allocation failures 400 - 499
    node_create_failure = 400


class BootnodeException(Exception):
    def __init__(self, error_code, *args, **kwargs):
        self.__error_code = error_code
        super(BootnodeException, self).__init__(*args, **kwargs)

    @property
    def error_code(self):
        return self.__error_code


class InvalidStateException(Exception):
    pass
# endregion
