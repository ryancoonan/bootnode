# region Imports
from __future__ import absolute_import
# django
from django.db import models
from django.contrib.auth.models import User
# bootnode
from common.models import BootnodeModel
# endregion


# region Public
class UserLockProcessed(BootnodeModel):
    """Table for tracking which users' resources have been freed"""

    user = models.ForeignKey(User)
# endregion
