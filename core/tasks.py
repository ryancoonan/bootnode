# region Imports
from __future__ import absolute_import
# django
from django.utils import timezone
# bootnode
from metadata.models import UserDetail
# internal
from core.models import UserLockProcessed
from core.repository.hosts import *
from core.repository.images import *
from core.repository.kits import *
from core.repository.nodes import *
# endregion


# region Public
@app.task
def free_locked_out_resources():
    users_to_lock = UserDetail.objects.filter(email_confirmed=False, deleted=None, lockout_at__lt=timezone.now())\
        .exclude(id__in=UserLockProcessed.objects.all())

    for user_detail in users_to_lock:
        lock_user_resources.apply_async((user_detail.id,))


@app.task
def lock_user_resources(user_id):
    user = User.from_id(user_id)
    user_nodes = get_nodes(user.django_auth_user)
    for node in user_nodes:
        if node.public_ipv4:
            node.free_public_ipv4()
        node.action('power-off')

    user_lock_processed = UserLockProcessed.objects.create(user=user.django_auth_user)
    user_lock_processed.save()

# endregion
