# cross-site request forgery protection
csrfSafeMethod = (method) ->
  # these HTTP methods do not require CSRF protection
  return /^(GET|HEAD|OPTIONS|TRACE)$/.test method

$.ajaxSetup {
  beforeSend: (xhr, settings) ->
    if not csrfSafeMethod(settings.type) and not @crossDomain
      csrfToken = $.cookie 'csrftoken'
      xhr.setRequestHeader('X-CSRFToken', csrfToken)
}

# utility functions
window.transitionText = ($id, text, midaction) ->
  $id.fadeOut(400, ->
    $id.text text
    midaction?()
    $id.fadeIn())

# string format ala '{0} Mr. {1}'.format('Hello,', 'Smith')
String::format = (args...) ->
  @replace /{(\d+)}/g, (match, number) ->
    if number < args.length then args[number] else match

# left-pad a number
# source: http://stackoverflow.com/a/1268377/1532702
Number::leftZeroPad = (numZeros) ->
  n = Math.abs(@)
  zeros = Math.max(0, numZeros - n.toString().length)
  zeroString = Math.pow(10, zeros).toString().substr(1)
  if @ < 0
    zeroString = "-#{zeroString}"
  return zeroString + n
