# region Imports
from __future__ import absolute_import
# standard lib
from time import sleep
# bootnode
from metadata.models import Node as BnNode, Flavor, Task as BnTask
from openstack.exceptions import OpenStackRequestException
from openstack.flavors import get_or_create_flavor_by_name
from openstack.images import get_image_by_name
from openstack.networking import create_security_group, attach_floating_ip
from openstack.servers import create_server
# internal
from core.celery import app
from core.domain.kit import Kit
from core.domain.node import Node
from core.domain.node_configuration import NodeConfiguration
from core.domain.types import TaskType
from core.domain.user import User
from core.exceptions import BootnodeException, BootnodeError
from core.repository.hosts import get_user_hosts_list
# endregion


# region Public
def build_node_async(user, node_configuration, kit=None):
    bn_node = BnNode.objects.create(
        owner_id=user.id,
        name=node_configuration.name,
        image_id=node_configuration.image_id,
        flavor_id=node_configuration.bootnode_node_configuration.flavor.id,
        ssh_key_id=user.ssh_key.id,
        kit_id=kit.id,
        source_configuration_id=node_configuration.id
    )
    bn_node.save()
    task = _build_node.apply_async((user.id, bn_node.id, node_configuration.id,
                                    kit.id if kit is not None else None))
    bn_node.task = task.id
    bn_node.save()

    bn_task = BnTask.objects.create(id=task.id, requester=user.django_auth_user, type=int(TaskType.build_node))
    bn_task.save()

    return Node.from_building_node(bn_node)
# endregion


# region Private
@app.task
def _build_node(user_id, bn_node_id, node_configuration_id, kit_id=None):
    # gather components
    node = Node.from_id(bn_node_id)
    node_configuration = NodeConfiguration.from_id(node_configuration_id)
    kit = Kit.from_id(kit_id) if kit_id is not None else None
    user = User.from_id(user_id)
    flavor = Flavor.get_or_create_from_configuration(node_configuration.cpus, node_configuration.disk,
                                                     node_configuration.memory)
    server_name = '{0}.{1}'.format(node_configuration.name, user.email)

    hosts_list = get_user_hosts_list(user)
    done = False
    while not done and len(hosts_list) > 0:
        os_server, host = _create_server_first_host(flavor, hosts_list, node_configuration, server_name, user)
        node.server_id = os_server['id']
        node.host_id = host.id
        node.save()
        secs = 1
        while node.status == 'BUILD':
            sleep(secs)
            secs += 1
            node = Node.from_id(bn_node_id)

        if node.status != 'ERROR':
            if user.home_host_id is None:
                user.home_host_id = host.id
                user.save()
            if node_configuration.has_public_ip:
                attach_floating_ip(host.name, node.private_ipv4)
            done = True

    if not done:
        raise BootnodeException(BootnodeError.node_create_failure)

    node.task_id = None
    node.save()


def _create_server_first_host(flavor, hosts_list, node_configuration, server_name, user):
    host = None
    os_server = None
    while os_server is None:
        host = hosts_list.pop()
        os_image = get_image_by_name(host.name, node_configuration.image.name)
        os_flavor = get_or_create_flavor_by_name(host.name, flavor.name)
        security_group = create_security_group(host.name, server_name, rules=node_configuration.get_security_rules())
        try:
            os_server = create_server(host.name, server_name, os_image['id'], os_flavor['id'], security_group['id'],
                                      public_rsa_key=user.public_key, admin_pass=user.console_password)
        except OpenStackRequestException:
            if len(hosts_list) > 0:
                continue
            else:
                raise
    return os_server, host


# endregion


def create_node(user, name, image_name, flavor_name):
    hosts_list = get_user_hosts_list(user)
    host = hosts_list.pop()
    node = None
    # TODO better logic with deferred tasks
    while node is None:
        try:
            node = Node.create(user, name, image_name, flavor_name, host)
        except OpenStackRequestException:
            if len(hosts_list) > 0:
                host = hosts_list.pop()
                continue
            else:
                raise

    # if the user has no home host pinned, this is their home host now
    if user.home_host is None:
        user.home_host = node.host
        user.save()
    return node


def get_nodes(dj_auth_user):
    """Get all nodes that user has access to"""
    bn_nodes = BnNode.objects.filter(owner=dj_auth_user, deleted=None)
    return filter(lambda node: node is not None, map(Node.from_bootnode_node, bn_nodes))
