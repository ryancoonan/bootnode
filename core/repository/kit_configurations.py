# region Imports
from __future__ import absolute_import
# bootnode
from metadata.models import KitConfiguration as BnKitConfiguration
# internal
from core.domain.kit_configuration import KitConfiguration
# endregion


# region Public
def get_kit_configurations():
    bn_kit_configurations = BnKitConfiguration.objects.filter(deleted=None)
    kit_configurations = map(KitConfiguration.from_bootnode_kit, bn_kit_configurations)
    return kit_configurations
# endregion
