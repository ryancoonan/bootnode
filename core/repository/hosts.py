# region Imports
from __future__ import absolute_import
# standard lib
from random import shuffle
# bootnode
from metadata.models import Host as BnHost
# internal
from core.domain.host import Host
# endregion


# region Public
def get_hosts():
    bn_hosts = BnHost.objects.filter(deleted=None)
    return map(Host.from_bootnode_host, bn_hosts)


def get_user_hosts_list(user):
    if user.home_host is not None:
        hosts_list = [user.home_host]
    else:
        hosts_list = get_hosts()
        shuffle(hosts_list)
    return hosts_list
# endregion
