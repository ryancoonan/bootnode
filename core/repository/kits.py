# region Imports
from __future__ import absolute_import
# standard lib
from collections import Counter
# bootnode
from core.domain.node import Node
from metadata.models import Kit as BnKit, Node as BnNode
# internal
from core.domain.kit import Kit
from core.domain.node_configuration import NodeConfiguration
from core.repository.nodes import build_node_async
# endregion


# region Public
def build_kit(user, kit_configuration):
    user_built_kits = BnKit.objects.filter(owner=user.django_auth_user,
                                           kit_configuration=kit_configuration.bootnode_kit_configuration,
                                           deleted=None)

    if len(user_built_kits) > 0:
        bn_kit = user_built_kits.first()
        bn_nodes = BnNode.objects.filter(owner=user.django_auth_user, kit=bn_kit, deleted=None)
        missing_node_configurations = _find_missing_node_configurations(kit_configuration, bn_nodes)
        nodes = map(Node.from_bootnode_node, bn_nodes)
        nodes += [build_node_async(user, NodeConfiguration.from_id(node_configuration_id), bn_kit)
                  for node_configuration_id
                  in missing_node_configurations]
    else:
        bn_kit = BnKit.objects.create(owner=user.django_auth_user,
                                      kit_configuration=kit_configuration.bootnode_kit_configuration)
        bn_kit.save()
        nodes = [build_node_async(user, node_configuration, bn_kit)
                 for node_configuration
                 in kit_configuration.node_configurations]


    return Kit.from_building_kit(bn_kit, nodes)


def get_kits(django_auth_user):
    """Get all kits this user has built"""
    bn_kits = BnKit.objects.filter(owner=django_auth_user, deleted=None)
    kits = map(Kit.from_bootnode_kit, bn_kits)
    return kits
# endregion


# region Private
def _find_missing_node_configurations(kit_configuration, bn_nodes):
    missing_configurations = []
    bn_node_configuration_ids = map(lambda node: node.source_configuration_id, bn_nodes)
    required_configurations = Counter([node_configuration.id
                                       for node_configuration
                                       in kit_configuration.node_configurations])
    for node_configuration_id in bn_node_configuration_ids:
        if node_configuration_id in required_configurations:
            required_configurations.subtract([node_configuration_id])
    for required_configuration_id, left in required_configurations.iteritems():
        for _ in xrange(0, left):
            missing_configurations.append(required_configuration_id)
    return missing_configurations
# endregion
