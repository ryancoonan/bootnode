# region Imports
from __future__ import absolute_import
# bootnode
from django.conf import settings
from metadata.models import Image as BnImage
from openstack.flavors import get_flavors
# internal
from core.domain.image import Image
# endregion


# region Public
def get_flavors_():
    """This will be removed as soon as real point-buy system is in place"""
    os_flavors = get_flavors(settings.BOOTNODE_MASTER_CLOUD_HOST)
    return filter(lambda f: f['name'] == 'bn_free', [{
        'id': os_flavor['id'],
        'disk': os_flavor['disk'],
        'vcpus': os_flavor['vcpus'],
        'ram': os_flavor['ram'],
        'name': os_flavor['name']
    } for os_flavor in os_flavors])


def get_images(user=None):
    """Get all available images, optionally just what a user can see"""
    # TODO logic for images constrained by user access
    images = map(Image.from_bootnode_image, BnImage.objects.filter(deleted=None))
    return filter(lambda image: image is not None, images)
# endregion
