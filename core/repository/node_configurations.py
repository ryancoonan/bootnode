# region Imports
from __future__ import absolute_import
# bootnode
from metadata.models import NodeConfiguration as BnNodeConfiguration
# internal
from core.domain.node_configuration import NodeConfiguration
# endregion


# region Public
def get_node_configurations():
    bn_node_configurations = BnNodeConfiguration.objects.filter(deleted=None)
    node_configurations = map(NodeConfiguration.from_bn_node_configuration, bn_node_configurations)
    return node_configurations
# endregion
