# region Imports
from __future__ import absolute_import
# bootnode
from metadata.models import Task as BnTask
# internal
from core.domain.task import Task
# endregion


# region Public
def get_tasks(dj_auth_user):
    bn_tasks = BnTask.objects.filter(requester=dj_auth_user, deleted=None)
    return map(Task.from_bootnode_task, bn_tasks)
# endregion
