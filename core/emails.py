# region Imports
from __future__ import absolute_import
# standard lib
import os
# django
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
# endregion


# region Public
def send_register_email(user):
    if settings.BOOTNODE_ENV == 'local':
        return

    email = user.email
    if settings.BOOTNODE_ENV == 'staging' and not email.endswith('@{0}'.format(settings.BOOTNODE_MAIL_HOST)):
        return

    email_subject = 'Welcome to bootnode, {username_part}!'.format(username_part=email.split('@')[0])
    email_body_template = open(os.path.join(settings.BASE_DIR, 'website/templates/website/emails/welcome.html'), 'r').read()
    email_body = email_body_template.replace('VALIDATION_CODE_PLACEHOLDER', user.email_confirmation_code)\
        .replace('CONSOLE_PASSWORD_PLACEHOLDER', user.console_password)

    _send_email([email], email_subject, email_body, bcc=settings.BOOTNODE_MAIL_WELCOME_BCCS)
# endregion


# region Private
def _send_email(email_addresses, subject, html_body, text_body='', from_email=None, bcc=None):
    if from_email is None:
        from_email = settings.BOOTNODE_MAIL_DEFAULT_SENDER

    message = EmailMultiAlternatives(subject=subject, body=text_body, from_email=from_email, to=email_addresses,
                                     bcc=bcc)
    message.attach_alternative(html_body, 'text/html')
    message.send()
# endregion
