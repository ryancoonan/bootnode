# region Imports
from __future__ import absolute_import
# bootnode
from metadata.models import Host as BnHost
# endregion


# region Public
from common import utils


class Host(object):
    """A backend host

    Public properties:
        property name       type        permissions
        -------------       ----        -----------
        datacenter          str         read
        id                  int         read
        name                str         read
        region              str         read
    """

    # region Setup Methods
    def __init__(self, fields, bn_host=None):
        self.__bn_host = bn_host

        self.__id = fields.get('id')
        self.__name = fields.get('name')
        self.__region = fields.get('region')
        self.__datacenter = fields.get('datacenter')

    # endregion


    # region Properties

    # datacenter
    @property
    def datacenter(self):
        return self.__datacenter


    # id
    @property
    def id(self):
        return self.__id


    # name
    @property
    def name(self):
        return self.__name


    # region
    @property
    def region(self):
        return self.__region

    # endregion


    # region Public Class Methods
    @classmethod
    def from_bootnode_host(cls, bn_host):
        return cls(Host.__dict_from_bootnode_host(bn_host), bn_host)

    @classmethod
    def from_id(cls, id_):
        bn_host = BnHost.objects.get(id=id_)
        return cls.from_bootnode_host(bn_host)

    # endregion


    # region Private Static Methods
    @staticmethod
    def __dict_from_bootnode_host(bn_host):
        return utils.model_dict(bn_host, fields=['id', 'name', 'region', 'datacenter'])

    # endregion

# endregion
