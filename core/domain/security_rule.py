# region Imports
from __future__ import absolute_import
# bootnode
from metadata.models import SecurityRule as BnSecurityRule
# internal
from core.domain.changeset import ChangeSet
# endregion


# region Public
class SecurityRule(object):
    """Represents a single firewall rule

    Public properties:
        property name       type        permissions
        -------------       ----        -----------
        direction           str         read/write
        ethertype           str         read/write
        id                  int         read
        port                int         read/write
        protocol            str         read/write
    """

    # region Setup
    def __init__(self, fields, bn_security_rule=None):
        self.__bn_security_rule = bn_security_rule

        self.__direction = fields.get('direction')
        self.__ethertype = fields.get('ethertype')
        self.__id = fields.get('id')
        self.__port = fields.get('port')
        self.__protocol = fields.get('protocol')

        self.__changeset = ChangeSet()

    # endregion


    # region Properties

    # direction
    @property
    def direction(self):
        return self.__direction

    @direction.setter
    def direction(self, value):
        self.__changeset.add('direction', self.__direction, value)
        self.__direction = value

    # ethertype
    @property
    def ethertype(self):
        return self.__ethertype

    @ethertype.setter
    def ethertype(self, value):
        self.__changeset.add('ethertype', self.__ethertype, value)
        self.__ethertype = value

    # id
    @property
    def id(self):
        return self.__id

    # port
    @property
    def port(self):
        return self.__port

    @port.setter
    def port(self, value):
        self.__changeset.add('port', self.__port, value)
        self.__port = value

    # protocol
    @property
    def protocol(self):
        return self.__protocol

    @protocol.setter
    def protocol(self, value):
        self.__changeset.add('protocol', self.__protocol, value)
        self.__protocol = value

    # endregion


    # region Public Instance Methods
    def as_dict(self):
        """Export properties as dict"""
        return {
            'direction': self.direction,
            'ethertype': self.ethertype,
            'id': self.id,
            'port': self.port,
            'protocol': self.protocol
        }
    # endregion


    # region Public Class Methods
    @classmethod
    def from_bootnode_security_rule(cls, bn_security_rule):
        return cls(cls.__dict_from_bn_security_rule(bn_security_rule), bn_security_rule)

    @classmethod
    def from_id(cls, security_rule_id):
        bn_security_rule = BnSecurityRule.objects.get(id=security_rule_id)
        return cls.from_bootnode_security_rule(bn_security_rule)
    # endregion


    # region Private Static Methods
    @staticmethod
    def __dict_from_bn_security_rule(bn_security_rule):
        return {
            'id': bn_security_rule.id,
            'port': bn_security_rule.port,
            'direction': bn_security_rule.direction,
            'protocol': bn_security_rule.protocol,
            'ethertype': bn_security_rule.ethertype
        }
    # endregion

# endregion
