# region Imports
from __future__ import absolute_import
# standard lib
from copy import copy
# bootnode
from metadata.models import Kit as BnKit
# internal
from core.domain.changeset import ChangeSet
from core.domain.host import Host
from core.domain.kit_configuration import KitConfiguration
from core.domain.node import Node
from core.domain.user import User
# endregion


# region Public
class Kit(object):
    """An instance of a kit

    Public properties:
        property name       type                permissions
        -------------       ----                -----------
        configuration       KitConfiguration    read
        configuration_id    int                 read
        host                Host                read
        host_id             int                 read
        id                  int                 read
        nodes               list[Node]          read
        owner               User                read
        owner_id            int                 read
    """


    # region Setup
    def __init__(self, fields, bn_kit=None):
        self.__bn_kit = bn_kit

        self.__configuration = fields.get('configuration')
        self.__configuration_id = (fields.get('configuration_id')
                                   if self.__configuration is None
                                   else self.__configuration.id)
        self.__host = fields.get('host')
        self.__host_id = fields.get('host_id') if self.__host is None else self.__host.id
        self.__id = fields.get('id')
        self.__nodes = fields.get('nodes')
        self.__owner = fields.get('owner')
        self.__owner_id = fields.get('owner_id') if self.__owner is None else self.__owner.id

        self.__changeset = ChangeSet()

    # endregion


    # region Properties

    # configuration
    @property
    def configuration(self):
        if self.__configuration is None and self.__configuration_id is not None:
            self.__configuration = KitConfiguration.from_id(self.__configuration_id)
        return self.__configuration

    # configuration_id
    @property
    def configuration_id(self):
        if self.__configuration_id is None and self.__configuration is not None:
            self.__configuration_id = self.__configuration.id
        return self.__configuration_id

    # host
    @property
    def host(self):
        if self.__host is None and self.__host_id is not None:
            self.__host = Host.from_id(self.__host_id)
        return self.__host

    # host_id
    @property
    def host_id(self):
        if self.__host_id is None and self.__host is not None:
            self.__host_id = self.__host.id
        return self.__host_id

    # id
    @property
    def id(self):
        return self.__id

    # nodes
    @property
    def nodes(self):
        return copy(self.__nodes)

    # owner
    @property
    def owner(self):
        if self.__owner is None and self.__owner_id is not None:
            self.__owner = User.from_id(self.__owner_id)
        return self.__owner

    # owner_id
    @property
    def owner_id(self):
        if self.__owner_id is None and self.__owner is not None:
            self.__owner_id = self.__owner.id
        return self.__owner_id

    # endregion


    # region Public Instance Methods
    def to_dict(self):
        return {
            'id': self.id,
            'configuration_id': self.configuration_id,
            'host_id': self.host_id,
            'node_ids': [node.id for node in self.nodes],
            'owner_id': self.owner_id
        }
    # endregion


    # region Public Class Methods
    @classmethod
    def from_building_kit(cls, bn_kit, nodes):
        return cls(Kit.__dict_from_building_kit(bn_kit, nodes), bn_kit)

    @classmethod
    def from_bootnode_kit(cls, bn_kit):
        return cls(Kit.__dict_from_bn_kit(bn_kit), bn_kit)

    @classmethod
    def from_id(cls, id_):
        bn_kit = BnKit.objects.get(id=id_)
        return cls.from_bootnode_kit(bn_kit)

    # endregion


    # region Private Static Methods
    @staticmethod
    def __dict_from_bn_kit(bn_kit):
        return {
            'id': bn_kit.id,
            'configuration_id': bn_kit.kit_configuration_id,
            'host_id': bn_kit.host_id,
            'nodes': filter(lambda node: node is not None, map(Node.from_bootnode_node, bn_kit.node_set.all())),
            'owner_id': bn_kit.owner_id
        }

    @staticmethod
    def __dict_from_building_kit(bn_kit, nodes):
        return {
            'id': bn_kit.id,
            'configuration_id': bn_kit.kit_configuration_id,
            'nodes': nodes,
            'owner_id': bn_kit.owner_id
        }
    # endregion

# endregion
