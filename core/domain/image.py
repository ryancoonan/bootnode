# region Imports
from __future__ import absolute_import
# bootnode
from datetime import datetime
from django.conf import settings
import pytz
from common.utils import datetime_to_unix_timestamp
from metadata.models import Image as BnImage
from openstack.images import get_image_by_name
# internal
from core.domain.changeset import ChangeSet
from core.domain.document import Document
from core.domain.types import ImageCategory
from core.domain.user import User
# endregion


# region Public
class Image(object):
    """A cloud image

    Public properties:
        property name       type            permissions
        -------------       ----            -----------
        application_cta     str             read/write
        application_name    str             read/write
        application_target  str             read/write
        bootnode_image      models.Image    read
        category            str             read/write
        created             datetime        read
        creator             User            read
        default_doc         Document        read/write
        default_doc_id      int             read/write
        display_name        str             read/write
        distro              str             read
        distro_version      str             read
        icon                str             read/write
        id                  int             read
        minimum_disk        int             read/write
        minimum_memory      int             read/write
        updated             datetime        read
        user_created        bool            read
    """

    # region Setup Methods
    def __init__(self, fields, bn_image, os_image):
        self.__bn_image = bn_image
        self.__os_image = os_image
        self.__bn_application = bn_image.application

        if self.__bn_application is not None:
            self.__application_cta = self.__bn_application.cta
            self.__application_name = self.__bn_application.name
            self.__application_target = self.__bn_application.target
        else:
            self.__application_cta = None
            self.__application_name = None
            self.__application_target = None

        self.__category = fields.get('category')
        self.__created = fields.get('created')
        self.__creator = fields.get('creator')
        self.__creator_id = fields.get('creator_id')
        self.__default_doc = fields.get('default_doc')
        self.__default_doc_id = fields.get('default_doc_id')
        self.__display_name = fields.get('display_name')
        self.__distro = fields.get('distro')
        self.__distro_version = fields.get('distro_version')
        self.__icon = fields.get('icon')
        self.__id = fields.get('id')
        self.__minimum_disk = fields.get('minimum_disk')
        self.__minimum_memory = fields.get('minimum_memory')
        self.__name = fields.get('name')
        self.__openstack_name = fields.get('openstack_name')
        self.__updated = fields.get('updated')
        self.__user_created = fields.get('user_created')

        self.__changeset = ChangeSet()

    # endregion


    # region Public Properties

    # application_cta
    @property
    def application_cta(self):
        return self.__application_cta

    @application_cta.setter
    def application_cta(self, value):
        self.__changeset.add('application_cta', self.__application_cta, value)
        self.__application_cta = value


    # application_cta
    @property
    def application_name(self):
        return self.__application_name

    @application_name.setter
    def application_name(self, value):
        self.__changeset.add('application_name', self.__application_name, value)
        self.__application_name = value


    # application_target
    @property
    def application_target(self):
        return self.__application_target

    @application_target.setter
    def application_target(self, value):
        self.__changeset.add('application_target', self.__application_target, value)
        self.__application_target = value


    # bootnode_image
    @property
    def bootnode_image(self):
        if self.__bn_image is None:
            self.__bn_image = BnImage.objects.get(id=self.id)
        return self.__bn_image

    # category
    @property
    def category(self):
        return self.__category

    @category.setter
    def category(self, value):
        self.__changeset.add('category', self.__category, value)
        self.__category = value


    # created
    @property
    def created(self):
        return self.__created


    # creator
    @property
    def creator(self):
        if self.__creator is None and self.__creator_id is not None:
            self.__creator = User.from_id(self.__creator_id)
        return self.__creator


    # default_doc
    @property
    def default_doc(self):
        if self.__default_doc is None and self.__default_doc_id is not None:
            self.__default_doc = Document.from_id(self.__default_doc_id)
        return self.__default_doc

    @default_doc.setter
    def default_doc(self, value):
        self.__changeset.add('default_doc', self.__default_doc, value)
        self.__default_doc = value


    # default_doc_id
    @property
    def default_doc_id(self):
        if self.__default_doc is not None:
            return self.__default_doc.id
        else:
            return None

    @default_doc_id.setter
    def default_doc_id(self, value):
        self.default_doc = Document.from_id(value)


    # display_name
    @property
    def display_name(self):
        return self.__display_name

    @display_name.setter
    def display_name(self, value):
        self.__changeset.add('display_name', self.__display_name, value)
        self.__display_name = value


    # distro
    @property
    def distro(self):
        return self.__distro


    # distro_version
    @property
    def distro_version(self):
        return self.__distro_version


    # icon
    @property
    def icon(self):
        return self.__icon

    @icon.setter
    def icon(self, value):
        self.__changeset.add('icon', self.__icon, value)
        self.__icon = value


    # id
    @property
    def id(self):
        return self.__id


    # minimum_disk
    @property
    def minimum_disk(self):
        return self.__minimum_disk

    @minimum_disk.setter
    def minimum_disk(self, value):
        self.__changeset.add('minimum_disk', self.__minimum_disk, value)
        self.__minimum_disk = value


    # minimum_disk
    @property
    def minimum_memory(self):
        return self.__minimum_memory

    @minimum_memory.setter
    def minimum_memory(self, value):
        self.__changeset.add('minimum_memory', self.__minimum_memory, value)
        self.__minimum_memory = value


    # name
    @property
    def name(self):
        return self.__name


    # updated
    @property
    def updated(self):
        return self.__updated


    # user_created
    @property
    def user_created(self):
        return self.__user_created

    # endregion


    # region Public Instance Methods
    def save(self):
        return

    def to_dict(self):
        return {
            'category': int(self.category),
            'created': datetime_to_unix_timestamp(self.created),
            'creator_id': self.__creator_id,
            'default_doc_id': self.default_doc_id,
            'display_name': self.display_name,
            'distro': self.distro,
            'distro_version': self.distro_version,
            'icon': self.icon,
            'id': self.id,
            'minimum_disk': self.minimum_disk,
            'minimum_memory': self.minimum_memory,
            'name': self.name,
            'updated': datetime_to_unix_timestamp(self.updated),
            'user_created': self.user_created,
            'application_cta': self.application_cta,
            'application_name': self.application_name,
            'application_target': self.application_target
        }

    # endregion


    # region Public Class Methods
    @classmethod
    def from_bootnode_image(cls, bn_image, host_name=None):
        if host_name is None:
            host_name = settings.BOOTNODE_MASTER_CLOUD_HOST
        os_image = get_image_by_name(host_name, bn_image.name)
        if os_image:
            return cls.from_components(bn_image, os_image)
        else:
            return None

    @classmethod
    def from_bootnode_image_id(cls, host_name, bn_image_id):
        bn_image = BnImage.objects.get(id=bn_image_id)
        return cls.from_bootnode_image(bn_image, host_name=host_name)

    @classmethod
    def from_components(cls, bn_image, os_image):
        return cls(cls.__combine_components(bn_image, os_image), bn_image, os_image)

    # endregion


    # region Private Static Methods
    @staticmethod
    def __combine_components(bn_image, os_image):
        return {
            'id': bn_image.id,
            'name': bn_image.name,
            'display_name': bn_image.display_name,
            'distro': bn_image.distro,
            'distro_version': bn_image.version,
            'user_created': bn_image.user_created,
            'creator': bn_image.creator if bn_image.user_created else None,
            'created': datetime.strptime(os_image.get('created_at'), settings.OPENSTACK_DATETIME_FORMAT).replace(
                tzinfo=pytz.utc),
            'minimum_disk': os_image.get('min_disk'),
            'minimum_memory': os_image.get('min_ram'),
            'openstack_name': os_image.get('name'),
            'updated': datetime.strptime(os_image.get('updated_at'), settings.OPENSTACK_DATETIME_FORMAT).replace(
                tzinfo=pytz.utc),
            'icon': bn_image.icon,
            'default_doc': bn_image.doc_page,
            'category': ImageCategory(bn_image.category),
            # TODO make true application domain object
            'application': bn_image.application
        }
        # endregion

# endregion
