# region Imports
from __future__ import absolute_import
# external
from celery.result import AsyncResult
# django
from django.conf import settings
from django.utils import timezone
# bootnode
from common.utils import datetime_to_unix_timestamp, descend_dict
from metadata.models import Node as BnNode
from openstack.exceptions import OpenStackRequestException
from openstack.flavors import get_flavor_by_id, get_flavor_by_name
from openstack.images import get_image_by_name
from openstack.networking import attach_floating_ip, free_floating_ip_by_address, delete_security_group, \
    create_security_group, default_security_group_rules
from openstack.servers import create_server, get_server, execute_server_action, delete_server, get_vnc_url
# internal
from core.domain.changeset import ChangeSet
from core.domain.host import Host
from core.domain.image import Image
from core.domain.task import Task
from core.domain.user import User
from core.exceptions import InvalidStateException, BootnodeError, BootnodeException
# endregion


# region Private Classes
class _ActionSpec(object):
    def __init__(self, states, os_action, os_payload=None):
        self.states = states
        self.os_action = os_action
        self.os_payload = os_payload

# endregion


# region Public
class Node(object):
    """A single node instance

    Public properties:
        property name       type        permissions
        -------------       ----        -----------
        changes             dict        read
        cpus                int         read/write
        created             datetime    read
        disk                float       read/write
        host                Host        read
        id                  str         read
        image               Image       read
        kit_id              int         read/write
        memory              int         read/write
        name                str         read/write
        owner               User        read
        private_ipv4        str         read
        public_ipv4         str         read
        security_group_id   str         read
        server_id           str         read/write
        status              str         read
        task                Task        read/write
        task_id             str         read/write
    """

    # region Setup Methods
    def __init__(self, fields, bn_node=None, os_server=None):
        self.__bn_node = bn_node
        self.__os_server = os_server

        self.__cpus = fields.get('cpus')
        self.__created = fields.get('created')
        self.__disk = fields.get('disk')
        self.__host = fields.get('host')
        self.__host_id = fields.get('host_id') if self.__host is None else self.__host.id
        self.__id = fields.get('id')
        self.__image = fields.get('image')
        self.__image_id = fields.get('image_id')
        self.__kit = fields.get('kit')
        self.__kit_id = fields.get('kit_id') if self.__kit is None else self.__kit.id
        self.__memory = fields.get('memory')
        self.__name = fields.get('name')
        self.__owner = fields.get('owner')
        self.__owner_id = fields.get('owner_id') if self.__owner is None else self.__owner.id
        self.__private_ipv4 = fields.get('private_ipv4')
        self.__public_ipv4 = fields.get('public_ipv4')
        self.__security_group_id = fields.get('security_group_id')
        self.__server_id = fields.get('server_id')
        self.__status = fields.get('status')
        self.__task = fields.get('task')
        self.__task_id = fields.get('task_id') if self.__task is None else self.__task.id

        self.__changeset = ChangeSet()
        self.__save_bn_model = False
        self.__save_os_model = False

    # endregion


    # region Public Properties

    # changes
    @property
    def changes(self):
        return self.__changeset.changes

    # cpus
    @property
    def cpus(self):
        return self.__cpus

    @cpus.setter
    def cpus(self, value):
        self.__changeset.add('cpus', self.__cpus, value)
        self.__cpus = value

    # created
    @property
    def created(self):
        return self.__created

    # disk
    @property
    def disk(self):
        return self.__disk

    @disk.setter
    def disk(self, value):
        self.__changeset.add('disk', self.__disk, value)
        self.__disk = value

    # host
    @property
    def host(self):
        if self.__host is None and self.__host_id is not None:
            self.__host = Host.from_id(self.__host_id)

        return self.__host

    # host_id
    @property
    def host_id(self):
        return self.__host_id

    @host_id.setter
    def host_id(self, value):
        host = Host.from_id(value)
        self.__changeset.add('host', self.__host, host)
        self.__host = host

    # id
    @property
    def id(self):
        return self.__id

    # image
    @property
    def image(self):
        if self.__image is None:
            hostname = self.host.name if self.host is not None else settings.BOOTNODE_MASTER_CLOUD_HOST
            self.__image = Image.from_bootnode_image_id(hostname, self.__image_id)
        return self.__image

    # kit_id
    @property
    def kit_id(self):
        if self.__kit_id is None and self.__kit is not None:
            self.__kit_id = self.__kit.id
        return self.__kit_id

    @kit_id.setter
    def kit_id(self, value):
        self.__changeset.add('kit_id', self.__kit_id, value)
        self.__kit_id = value
        self.__kit = None

    # memory
    @property
    def memory(self):
        return self.__memory

    @memory.setter
    def memory(self, value):
        self.__changeset.add('memory', self.__memory, value)
        self.__memory = value

    # name
    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, value):
        self.__changeset.add('name', self.__name, value)
        self.__name = value

    # owner
    @property
    def owner(self):
        if self.__owner is None:
            self.__owner = User.from_id(self.__owner_id)
        return self.__owner

    # private_ipv4
    @property
    def private_ipv4(self):
        return self.__private_ipv4

    # public_ipv4
    @property
    def public_ipv4(self):
        return self.__public_ipv4

    def add_public_ipv4(self):
        try:
            floatingip = attach_floating_ip(self.host.name, self.private_ipv4)
        except OpenStackRequestException:
            raise BootnodeException(BootnodeError.ip_allocation_failed)
        self.__public_ipv4 = floatingip['floating_ip_address']

    @public_ipv4.deleter
    def public_ipv4(self):
        free_floating_ip_by_address(self.host.name, self.__public_ipv4)
        self.__public_ipv4 = None

    # security_group_id
    @property
    def security_group_id(self):
        return self.__security_group_id

    # server_id
    @property
    def server_id(self):
        return self.__server_id

    @server_id.setter
    def server_id(self, value):
        self.__changeset.add('server_id', self.__server_id, value)
        self.__server_id = value

    # status
    @property
    def status(self):
        return self.__status

    # task
    @property
    def task(self):
        if self.__task is None and self.__task_id is not None:
            self.__task = Task.from_id(self.__task_id)
        return self.__task

    @task.setter
    def task(self, value):
        self.__changeset.add('task', self.task, value)
        self.__task = value
        self.__task_id = value.id

    # task_id
    @property
    def task_id(self):
        if self.__task_id is None and self.__task is not None:
            self.__task_id = self.__task.id
        return self.__task_id

    @task_id.setter
    def task_id(self, value):
        self.__changeset.add('task_id', self.task_id, value)
        self.__task_id = value
        self.__task = None
    # endregion


    # region Public Instance Methods
    def action(self, action):
        if action in self.__special_actions:
            self.__special_actions[action](self)
            return

        action_spec = self.__action_specs.get(action)
        if not action_spec or self.status not in action_spec.states:
            msg = 'cannot take action \'{0}\' on node in state \'{1}\''.format(action, self.status)
            raise InvalidStateException(msg)

        execute_server_action(self.host.name, self.server_id, action_spec.os_action, action_spec.os_payload)

    def check_status(self):
        if self.task is not None:
            # update task
            self.task = AsyncResult(self.task_id)
            self.__status = self.task.status
        else:
            os_server = get_server(self.host.name, self.server_id)
            self.__status = os_server['status']
        return self.__status

    def delete(self):
        if self.status == 'ACTIVE':
            self.action('power-off')
        if self.__public_ipv4:
            free_floating_ip_by_address(self.host.name, self.__public_ipv4)
        delete_server(self.host.name, self.server_id)
        self.__bn_node.deleted = timezone.now()
        self.__bn_node.name = 'DELETED_{0}_{1}'.format(self.__bn_node.name,
                                                       int(datetime_to_unix_timestamp(self.__bn_node.deleted)))

        try:
            delete_security_group(self.host.name, self.__security_group_id)
        except OpenStackRequestException:
            # TODO when we move to 100% deferred background jobs this issue will be resolved
            pass

        self.__bn_node.save()

    def free_public_ipv4(self):
        free_floating_ip_by_address(self.host.name, self.public_ipv4)

    def get_console_url(self):
        return get_vnc_url(self.host.name, self.server_id)

    def save(self):
        changes = self.__changeset.changes()
        for field, values in changes.iteritems():
            old, new = values
            self.__save_specs[field](self, new)

        if self.__save_bn_model:
            self.__bn_node.save()
        if self.__save_os_model:
            # TODO this
            pass

        self.__save_bn_model = False
        self.__save_os_model = False
        self.__changeset.clear()

    def to_dict(self):
        return {
            'cpus': self.cpus,
            'created': datetime_to_unix_timestamp(self.created),
            'disk': self.disk,
            'kit_id': self.kit_id,
            'id': self.id,
            'image_name': self.image.name,
            'memory': self.memory,
            'name': self.name,
            'owner': self.__owner_id,
            'private_ipv4': self.private_ipv4,
            'public_ipv4': self.public_ipv4,
            'server_id': self.server_id,
            'status': self.status,
            'task_id': self.task_id
        }
    # endregion


    # region Private Instance
    def __save_host(self, value):
        self.__save_host_id(value.id)

    def __save_host_id(self, value):
        self.__bn_node.host_id = value
        self.__save_bn_model = True

    def __save_name(self, value):
        self.__bn_node.name = value
        self.__save_bn_model = True

    def __save_server_id(self, value):
        self.__bn_node.server_id = value
        self.__save_bn_model = True

    def __save_task(self, value):
        self.__save_task_id(value.id)

    def __save_task_id(self, value):
        self.__bn_node.task_id = value
        self.__save_bn_model = True

    __action_specs = {
        'power-off': _ActionSpec(['ACTIVE'], 'os-stop'),
        'power-on': _ActionSpec(['SHUTOFF'], 'os-start'),
        'reboot': _ActionSpec(['ACTIVE'], 'reboot', {'type': 'SOFT'}),
        'suspend': _ActionSpec(['ACTIVE'], 'suspend'),
        'unsuspend': _ActionSpec(['SUSPENDED'], 'resume')
    }

    __save_specs = {
        'host': __save_host,
        'name': __save_name,
        'server_id': __save_server_id,
        'task': __save_task,
        'task_id': __save_task_id
    }

    __special_actions = {
        'get-ip': add_public_ipv4,
        'free-ip': free_public_ipv4
    }
    # endregion


    # region Public Class Methods
    @classmethod
    def create(cls, user, name, image_name, flavor_name, host):
        os_server_name = '{0}.{1}'.format(name, user.email)
        image = get_image_by_name(host.name, image_name)
        flavor = get_flavor_by_name(host.name, flavor_name)

        security_group_id = Node.__create_security_group(host.name, os_server_name)
        os_server = create_server(host.name, os_server_name, image['id'], flavor['id'], security_group_id,
                                  public_rsa_key=user.public_key, admin_pass=user.console_password)
        bn_node = BnNode.create_(os_server['id'], user.django_auth_user, name, image_name, flavor_name,
                                 security_group_id, host.id, save=True)

        return cls.from_bootnode_node(bn_node)

    @classmethod
    def from_bootnode_node(cls, bn_node):
        if bn_node.host is None:
            return cls.from_building_node(bn_node)
        os_server = get_server(bn_node.host.name, bn_node.server_id)
        if os_server:
            return cls.from_components(bn_node, os_server)
        else:
            return None

    @classmethod
    def from_building_node(cls, bn_node):
        return cls(Node.__combine_building_components(bn_node), bn_node)

    @classmethod
    def from_components(cls, bn_node, os_server):
        return cls(Node.__combine_components(bn_node, os_server), bn_node, os_server)

    @classmethod
    def from_id(cls, id_):
        bn_node = BnNode.objects.get(id=id_)
        return cls.from_bootnode_node(bn_node)

    # endregion


    # region Private Static Methods
    @staticmethod
    def __combine_building_components(bn_node):
        return {
            'created': bn_node.created,
            'id': bn_node.id,
            'name': bn_node.name,
            'status': 'BUILD',
            'image_id': bn_node.image_id,
            'owner_id': bn_node.owner_id,
            'kit_id': bn_node.kit.id if bn_node.kit is not None else None,
            'cpus': bn_node.flavor.cpus,
            'disk': bn_node.flavor.disk,
            'memory': bn_node.flavor.ram,
            'task_id': bn_node.task
        }

    @staticmethod
    def __combine_components(bn_node, os_server):
        node = {
            'created': bn_node.created,
            'id': bn_node.id,
            'name': bn_node.name,
            'status': os_server.get('status'),
            'image_id': bn_node.image_id,
            'security_group_id': bn_node.security_group_id,
            'owner_id': bn_node.owner_id,
            'host_id': bn_node.host_id,
            'kit_id': bn_node.kit_id,
            'server_id': os_server.get('id'),
            'task_id': bn_node.task
        }
        node.update(Node.__parse_ips_from_os_server(os_server))
        node.update(Node.__get_resources_by_flavor_id(bn_node.host.name, bn_node.flavor_id))
        return node

    @staticmethod
    def __create_security_group(host_name, security_group_name):
        # TODO actually customizable rules
        return create_security_group(host_name, security_group_name, rules=default_security_group_rules)['id']

    @staticmethod
    def __get_resources_by_flavor_id(host_name, flavor_id):
        flavor = get_flavor_by_id(host_name, flavor_id)
        return {
            'disk': flavor['disk'],
            'cpus': flavor['vcpus'],
            'memory': flavor['ram']
        }

    @staticmethod
    def __parse_ip_from_address(address):
        address_type = address.get('OS-EXT-IPS:type')
        if address_type is None:
            return {}

        field_prefix = {
            'fixed': 'private',
            'floating': 'public'
        }.get(address_type)

        ip_version = address['version']
        field_name = '{0}_ipv{1}'.format(field_prefix, ip_version)
        return {field_name: address['addr']}

    @staticmethod
    def __parse_ips_from_os_server(os_server):
        ips = {}

        addresses = os_server.get('addresses')
        if addresses is None or 'bn_private' not in addresses:
            return ips

        for address in addresses['bn_private']:
            ips.update(Node.__parse_ip_from_address(address))

        return ips
    # endregion

# endregion
