# region Imports
from __future__ import absolute_import
# endregion


# region Public
class ChangeSet(object):
    """A set of changed properties"""

    # region Setup Methods
    def __init__(self):
        self.__changeset = {}
    # endregion


    # region Public Instance Methods
    def add(self, field, old_value, new_value):
        if field not in self.__changeset:
            self.__changeset[field] = (old_value, new_value)
        else:
            self.__changeset[field] = (self.__changeset[field][0], new_value)

    def changed(self, field):
        return field in self.__changeset

    def changes(self):
        return self.__changeset.copy() if len(self.__changeset) != 0 else None

    def clear(self):
        self.__changeset = {}
    # endregion

# endregion
