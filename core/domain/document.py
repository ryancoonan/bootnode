# region Imports
from __future__ import absolute_import
# bootnode
from metadata.models import Document as BnDocument
# internal
from core.domain.changeset import ChangeSet
from core.domain.user import User
# endregion


# region Public
class Document(object):
    """A document

    Public properties:
        property name       type        privileges
        -------------       ----        ----------
        author              User        read
        body                str         read/write
        id                  int         read
        keywords            list[str]   read/write
        path                str         read
        pub_date            datetime    read/write
        title               str         read/write
    """

    # region Setup Methods
    def __init__(self, fields, bn_document):
        self.__bn_document = bn_document

        self.__author = fields.get('author')
        self.__author_id = fields.get('author_id')
        self.__body = fields.get('body')
        self.__id = fields.get('id')
        self.__keywords = set(fields.get('keywords', []))
        self.__path = fields.get('path')
        self.__pub_date = fields.get('pub_date')
        self.__title = fields.get('title')

        self.__changeset = ChangeSet()
    # endregion


    # region Public Properties

    # author
    @property
    def author(self):
        if self.__author is None:
            self.__author = User.from_id(self.__author_id)
        return self.__author


    # body
    @property
    def body(self):
        return self.__body

    @body.setter
    def body(self, value):
        self.__changeset.add('body', self.__body, value)


    # id
    @property
    def id(self):
        return self.__id


    # keywords
    @property
    def keywords(self):
        return frozenset(self.__keywords)

    def add_keyword(self, keyword):
        old_keywords = frozenset(self.__keywords)
        self.__keywords.add(keyword)
        self.__changeset.add('keywords', old_keywords, self.__keywords)


    # path
    @property
    def path(self):
        return self.__path


    # pub_date
    @property
    def pub_date(self):
        return self.__pub_date

    @pub_date.setter
    def pub_date(self, value):
        self.__changeset.add('pub_date', self.__pub_date, value)
        self.__pub_date = value


    # title
    @property
    def title(self):
        return self.__title

    @title.setter
    def title(self, value):
        self.__changeset.add('title', self.__title, value)
        self.__title = value
    # endregion


    # region Public Instance Methods
    def save(self):
        return
    # endregion


    # region Public Class Methods
    @classmethod
    def from_bootnode_document(cls, bn_document):
        return cls(cls.__dict_from_bn_document(bn_document), bn_document)

    @classmethod
    def from_id(cls, id_):
        bn_document = BnDocument.objects.get(id=id_)
        return cls.from_bootnode_document(bn_document)
    # endregion


    # region Private Static Methods
    @staticmethod
    def __dict_from_bn_document(bn_document):
        return {
            'id': bn_document.id,
            'author': bn_document.user,
            'title': bn_document.title,
            'body': bn_document.body,
            'pub_date': bn_document.pub_date,
            'path': bn_document.path,
            'keywords': [keyword.keyword_text for keyword in bn_document.keywords.all()]
        }
    # endregion

# endregion
