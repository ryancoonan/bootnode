# region Imports
from __future__ import absolute_import
# django
from datetime import timedelta
from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth.models import User as DjUser
# bootnode
from django.utils import timezone
from common.utils import random_string, datetime_to_unix_timestamp
from core.domain.host import Host
from metadata.models import UserDetail, SshKey
# internal
from core.domain.changeset import ChangeSet
# endregion


# region Public
class User(object):
    """A bootnode user

    Public properties:
        property name               type                                permissions
        -------------               ----                                -----------
        allowed_ips                 int                                 read/write
        allowed_nodes               int                                 read/write
        built_kits                  QuerySet[Kit]                       read/write
        console_password            str                                 read
        created                     datetime                            read
        django_auth_user            django.contrib.auth.models.User     read
        email                       str                                 read/write
        email_confirmation_code     str                                 read
        email_confirmed             bool                                read/write
        home_host                   Host                                read/write
        home_host_id                int                                 read/write
        id                          int                                 read
        is_locked_out               bool                                read
        lockout_at                  datetime                            read/write
        password                    str                                 write
        private_key                 str                                 read
        public_key                  str                                 read
        ssh_key                     BnSshKey                            read
    """

    # region Setup Methods
    def __init__(self, fields, dj_auth_user=None, bn_user_detail=None):
        self.__dj_auth_user = dj_auth_user
        self.__bn_user_detail = bn_user_detail
        self.__ssh_key = fields.get('ssh_key')

        self.__allowed_ips = fields.get('allowed_ips', 1)
        self.__allowed_nodes = fields.get('allowed_nodes', 3)
        self.__built_kits = fields.get('built_kits')
        self.__console_password = fields.get('console_password')
        self.__created = fields.get('created')
        self.__email = fields.get('email')
        self.__email_confirmation_code = fields.get('email_confirmation_code')
        self.__email_confirmed = fields.get('email_confirmed')
        self.__home_host = fields.get('home_host')
        self.__home_host_id = fields.get('home_host_id')
        if self.__home_host_id is None and self.__home_host is not None:
            self.__home_host_id = self.__home_host.id
        self.__id = fields.get('id')
        self.__lockout_at = fields.get('lockout_at')
        self.__ssh_key_id = fields.get('ssh_key_id')

        self.__changeset = ChangeSet()
        self.__save_bn_user_detail = False
        self.__save_dj_auth_user = False
    # endregion


    # region Properties
    # allowed_ips
    @property
    def allowed_ips(self):
        return self.__allowed_ips

    @allowed_ips.setter
    def allowed_ips(self, value):
        self.__changeset.add('allowed_ips', self.__allowed_ips, value)
        self.__allowed_ips = value


    # allowed_nodes
    @property
    def allowed_nodes(self):
        return self.__allowed_nodes

    @allowed_nodes.setter
    def allowed_nodes(self, value):
        self.__changeset.add('allowed_nodes', self.__allowed_nodes, value)
        self.__allowed_nodes = value


    # built_kits
    @property
    def built_kits(self):
        return self.__built_kits

    def add_built_kit(self, kit):
        # TODO this is untested
        self.__built_kits.add(kit)

    def save_built_kits(self):
        self.__bn_user_detail.save()


    # console_password
    @property
    def console_password(self):
        return self.__console_password


    # created
    @property
    def created(self):
        return self.__created


    # django_auth_user
    @property
    def django_auth_user(self):
        return self.__dj_auth_user


    # email
    @property
    def email(self):
        return self.__email

    @email.setter
    def email(self, value):
        self.__changeset.add('email', self.__email, value)
        self.__email = value

    # email_confirmation_code
    @property
    def email_confirmation_code(self):
        return self.__email_confirmation_code

    # email_confirmed
    @property
    def email_confirmed(self):
        return self.__email_confirmed

    @email_confirmed.setter
    def email_confirmed(self, value):
        self.__changeset.add('email_confirmed', self.__email_confirmed, value)
        self.__email_confirmed = value

    # home_host
    @property
    def home_host(self):
        if self.__home_host is None and self.__home_host_id is not None:
            self.__home_host = Host.from_id(self.__home_host_id)
        return self.__home_host

    @home_host.setter
    def home_host(self, value):
        self.__changeset.add('home_host', self.__home_host, value)
        self.__home_host = value

    # home_host_id
    @property
    def home_host_id(self):
        if self.__home_host_id is None and self.__home_host is not None:
            self.__home_host_id = self.__home_host.id
        return self.__home_host_id

    @home_host_id.setter
    def home_host_id(self, value):
        self.__changeset.add('home_host_id', self.home_host_id, value)
        self.__home_host_id = value
        self.__home_host = None

    # id
    @property
    def id(self):
        return self.__id

    # is_locked_out
    @property
    def is_locked_out(self):
        return not self.email_confirmed and self.lockout_at < timezone.now()

    # lockout_at
    @property
    def lockout_at(self):
        return self.__lockout_at

    @lockout_at.setter
    def lockout_at(self, value):
        self.__changeset.add('lockout_at', self.__lockout_at, value)
        self.__lockout_at = value

    # password
    @property
    def password(self):
        # cleartext passwords are not stored and authentication against hashes should be done within the Django auth
        # module
        return None

    @password.setter
    def password(self, value):
        self.__changeset.add('password', None, value)


    # private_key
    @property
    def private_key(self):
        self.__ssh_key = self.__get_ssh_key()
        if self.__ssh_key:
            return self.__ssh_key.private_key

        return None


    # public_key
    @property
    def public_key(self):
        self.__ssh_key = self.__get_ssh_key()
        if self.__ssh_key:
            return self.__ssh_key.public_key

        return None

    # ssh_key
    @property
    def ssh_key(self):
        return self.__ssh_key

    # endregion


    # region Public Instance Methods
    def clear_changes(self):
        self.__save_bn_user_detail = False
        self.__save_dj_auth_user = False
        self.__changeset.clear()

    def save(self):
        changes = self.__changeset.changes()
        self.__prepare_save(changes)
        self.__write_out_changes()
        self.clear_changes()

    def to_dict(self):
        return {
            'allowed_ips': self.allowed_ips,
            'allowed_nodes': self.allowed_nodes,
            'console_password': self.console_password,
            'created': datetime_to_unix_timestamp(self.created),
            'email': self.email,
            'id': self.id,
            'lockout_at': datetime_to_unix_timestamp(self.lockout_at),
            'is_locked_out': self.is_locked_out
        }
    # endregion


    # region Private Instance
    def __get_ssh_key(self):
        if not self.__ssh_key:
            if self.__ssh_key_id:
                return SshKey.objects.get(id=self.__ssh_key_id)
            else:
                return None

        return self.__ssh_key

    def __prepare_save(self, changes):
        for field, values in changes.iteritems():
            old, new = values
            self.__save_specs[field](self, new)

    def __save_email_confirmed(self, value):
        self.__bn_user_detail.email_confirmed = value
        self.__save_bn_user_detail = True

    def __save_home_host(self, value):
        self.__save_home_host_id(value.id if value else None)

    def __save_home_host_id(self, value):
        self.__bn_user_detail.home_host_id = value
        self.__save_bn_user_detail = True

    def __save_lockout_at(self, value):
        self.__bn_user_detail.lockout_at = value
        self.__save_bn_user_detail = True

    def __write_out_changes(self):
        if self.__save_bn_user_detail:
            self.__bn_user_detail.save()
        if self.__save_dj_auth_user:
            self.__dj_auth_user.save()

    __save_specs = {
        'email_confirmed': __save_email_confirmed,
        'home_host': __save_home_host,
        'home_host_id': __save_home_host_id,
        'lockout_at': __save_lockout_at
    }
    # endregion


    # region Public Class Methods
    @classmethod
    def from_bootnode_user_detail(cls, bn_user_detail):
        dj_auth_user = DjUser.objects.get(id=bn_user_detail.user_id)
        return cls.from_components(dj_auth_user, bn_user_detail)

    @classmethod
    def from_components(cls, dj_auth_user, bn_user_detail):
        return cls(cls.__combine_components(dj_auth_user, bn_user_detail), dj_auth_user, bn_user_detail)

    @classmethod
    def from_credentials(cls, username, password):
        dj_auth_user = authenticate(username=username, password=password)
        if dj_auth_user is None or not dj_auth_user.is_active:
            return None

        return cls.from_django_auth_user(dj_auth_user)

    @classmethod
    def from_django_auth_user(cls, dj_auth_user):
        bn_user_detail = UserDetail.objects.get(user=dj_auth_user)
        return cls.from_components(dj_auth_user, bn_user_detail)

    @classmethod
    def from_id(cls, id_):
        dj_auth_user = DjUser.objects.get(id=id_)
        return cls.from_django_auth_user(dj_auth_user)

    @classmethod
    def from_validation_code(cls, validation_code):
        bn_user_detail = UserDetail.objects.get(email_confirmation_code=validation_code)
        return cls.from_bootnode_user_detail(bn_user_detail)

    @classmethod
    def register(cls, email, password):
        dj_user = DjUser.objects.create_user(email, email, password)
        ssh_key = SshKey.create(dj_user)
        bn_user_detail = User.__create_user_detail(dj_user)

        dj_user.save()
        ssh_key.save()
        bn_user_detail.save()
        return cls.from_components(dj_user, bn_user_detail)

    # endregion


    # region Private Static Methods
    @staticmethod
    def __combine_components(dj_auth_user, bn_user_detail):
        return {
            'id': dj_auth_user.id,
            'email': dj_auth_user.email,
            'console_password': bn_user_detail.console_password,
            'created': dj_auth_user.date_joined,
            'allowed_nodes': bn_user_detail.allowed_nodes,
            'allowed_ips': bn_user_detail.allowed_ips,
            'ssh_key': SshKey.objects.get(user=dj_auth_user),
            'home_host': bn_user_detail.home_host,
            'email_confirmation_code': bn_user_detail.email_confirmation_code,
            'email_confirmed': bn_user_detail.email_confirmed,
            'lockout_at': bn_user_detail.lockout_at
        }

    @staticmethod
    def __create_user_detail(dj_user):
        console_password = random_string(8)
        email_confirmation_code = random_string(20)
        lockout_at = timezone.now() + timedelta(hours=settings.BOOTNODE_USER_VALIDATION_WINDOW)
        bn_user_detail = UserDetail.objects.create(user=dj_user, console_password=console_password,
                                                   email_confirmation_code=email_confirmation_code,
                                                   lockout_at=lockout_at)
        return bn_user_detail

    # endregion

# endregion
