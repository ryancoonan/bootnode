# region Imports
from __future__ import absolute_import
# standard lib
from copy import copy
# bootnode
from metadata.models import KitConfiguration as BnKit
# internal
from core.domain.changeset import ChangeSet
from core.domain.node_configuration import NodeConfiguration
# endregion


# region Public
class KitConfiguration(object):
    """A kit specification

    Public properties:
        property name               type                        permissions
        -------------               ----                        -----------
        bootnode_kit_configuration  BnKitConfiguration          read
        default_node                NodeConfiguration           read/write
        default_node_id             int                         read/write
        id                          int                         read
        landing_layout              str                         read/write
        link_postfix                str                         read/write
        name                        str                         read/write
        node_configurations         list[NodeConfiguration]     read/write
    """

    # region Setup
    def __init__(self, fields, bn_kit_configuration=None):
        self.__bn_kit_configuration = bn_kit_configuration

        self.__default_node = fields.get('default_node')
        self.__default_node_id = (fields.get('default_node_id')
                                  if self.__default_node is None
                                  else self.__default_node.id)
        self.__landing_layout = fields.get('landing_layout')
        self.__link_postfix = fields.get('link_postfix')
        self.__name = fields.get('name')
        self.__node_configurations = fields.get('node_configurations')
        self.__id = fields.get('id')

        self.__changeset = ChangeSet()
    # endregion


    # region Properties

    # bootnode_kit_configuration
    @property
    def bootnode_kit_configuration(self):
        return self.__bn_kit_configuration

    # default_node
    @property
    def default_node(self):
        if self.__default_node is None and self.__default_node_id is not None:
            self.__default_node = NodeConfiguration.from_id(self.__default_node_id)
        return self.__default_node

    @default_node.setter
    def default_node(self, value):
        self.__changeset.add('default_node', self.__default_node, value)
        self.__changeset.add('default_node_id', self.__default_node_id, value.id)
        self.__default_node = value
        self.__default_node_id = value.id

    # default_node_id
    @property
    def default_node_id(self):
        return self.__default_node_id

    @default_node_id.setter
    def default_node_id(self, value):
        self.__changeset.add('default_node_id', self.__default_node_id, value)
        self.__default_node_id = value
        self.__default_node = None

    # id
    @property
    def id(self):
        return self.__id

    # landing_layout
    @property
    def landing_layout(self):
        return self.__landing_layout

    @landing_layout.setter
    def landing_layout(self, value):
        self.__changeset.add('landing_layout', self.__landing_layout, value)
        self.__landing_layout = value

    # link_postfix
    @property
    def link_postfix(self):
        return self.__landing_layout

    @link_postfix.setter
    def link_postfix(self, value):
        self.__changeset.add('link_postfix', self.__link_postfix, value)
        self.__link_postfix = value

    # name
    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, value):
        self.__changeset.add('name', self.__name, value)
        self.__name = value

    # node_configurations
    @property
    def node_configurations(self):
        return copy(self.__node_configurations)

    def add_node_configuration(self, value):
        old_node_configurations = copy(self.__node_configurations)
        self.__node_configurations.append(value)
        self.__changeset.add('node_configurations', old_node_configurations, self.__node_configurations)

    # endregion


    # region Public Instance Methods
    def to_dict(self):
        return {
            'default_node_configuration_id': self.default_node_id,
            'id': self.id,
            'landing_layout': self.landing_layout,
            'link_postfix': self.link_postfix,
            'name': self.name,
            'node_configuration_ids': [node_configuration.id for node_configuration in self.node_configurations]
        }
    # endregion


    # region Public Class Methods
    @classmethod
    def from_bootnode_kit(cls, bn_kit):
        return cls(KitConfiguration.__dict_from_bn_kit(bn_kit), bn_kit)

    @classmethod
    def from_id(cls, id_):
        bn_kit = BnKit.objects.get(id=id_)
        return cls.from_bootnode_kit(bn_kit)

    @classmethod
    def from_link_postfix(cls, link_postfix):
        bn_kit = BnKit.objects.get(link_postfix=link_postfix, deleted=None)
        return cls.from_bootnode_kit(bn_kit)
    # endregion


    # region Private Static Methods
    @staticmethod
    def __dict_from_bn_kit(bn_kit):
        return {
            'default_node_id': bn_kit.default_node_id,
            'id': bn_kit.id,
            'landing_layout': bn_kit.landing_layout,
            'link_postfix': bn_kit.link_postfix,
            'name': bn_kit.name,
            'node_configurations': map(NodeConfiguration.from_bn_node_configuration, bn_kit.node_configurations.all())
        }
    # endregion

# endregion
