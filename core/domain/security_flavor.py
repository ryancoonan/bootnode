# region Imports
from __future__ import absolute_import
# standard lib
from copy import copy
# bootnode
from metadata.models import SecurityFlavor as BnSecurityFlavor
# internal
from core.domain.changeset import ChangeSet
from core.domain.security_rule import SecurityRule
# endregion


# region Public
class SecurityFlavor(object):
    """A collection of security rules

    Public properties:
        property name       type                permissions
        -------------       ----                -----------
        id                  int                 read
        name                str                 read/write
        rules               list[SecurityRule]  read/write
    """

    # region Setup
    def __init__(self, fields, bn_security_flavor=None):
        self.__bn_security_flavor = bn_security_flavor

        self.__id = fields.get('id')
        self.__rules = map(SecurityRule.from_bootnode_security_rule, fields.get('rules', []))
        self.__name = fields.get('name')

        self.__changeset = ChangeSet()

    # endregion


    # region Properties

    # id
    @property
    def id(self):
        return self.__id

    # rules
    @property
    def rules(self):
        return copy(self.__rules)

    def add_rule(self, value):
        old_rules = frozenset(self.__rules)
        self.__rules.append(value)
        self.__changeset.add('rules', old_rules, self.__rules)

    # name
    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, value):
        self.__changeset.add('name', self.__name, value)
        self.__name = value

    # endregion


    # region Public Class Methods
    @classmethod
    def from_bootnode_security_flavor(cls, bn_security_flavor):
        return cls(SecurityFlavor.__dict_from_bn_security_flavor(bn_security_flavor), bn_security_flavor)

    @classmethod
    def from_id(cls, id_):
        bn_security_flavor = BnSecurityFlavor.objects.get(id=id_)
        return cls.from_bootnode_security_flavor(bn_security_flavor)

    # endregion


    # region Private Static Methods
    @staticmethod
    def __dict_from_bn_security_flavor(bn_security_flavor):
        return {
            'id': bn_security_flavor.id,
            'name': bn_security_flavor.name,
            'rules': bn_security_flavor.rules.all()
        }

    # endregion

# endregion
