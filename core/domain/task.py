# region Imports
from __future__ import absolute_import
# external
from celery.result import AsyncResult
# bootnode
from common.utils import datetime_to_unix_timestamp
from metadata.models import Task as BnTask
# internal
from core.domain.user import User
# endregion


# region Public
class Task(object):
    """A pending task

    Public properties:
        property name       type            permissions
        -------------       ----            -----------
        created             datetime        read
        id                  str             read
        requester           User            read
        requester_id        int             read
        status              str             read
        type                TaskType        read
    """


    # region Setup
    def __init__(self, fields, celery_task=None, bootnode_task=None):
        self.__celery_task = celery_task
        self.__bootnode_task = bootnode_task

        self.__created = fields.get('created')
        self.__id = fields.get('id')
        self.__requester = fields.get('requester')
        self.__requester_id = fields.get('requester_id') if self.__requester is None else self.__requester.id
        self.__status = fields.get('status')
        self.__type = fields.get('type')
    # endregion


    # region Properties

    # created
    @property
    def created(self):
        return self.__created

    # id
    @property
    def id(self):
        return self.__id

    # requester
    @property
    def requester(self):
        if self.__requester is None and self.__requester_id is not None:
            self.__requester = User.from_id(self.__requester_id)
        return self.__requester

    # requester_id
    @property
    def requester_id(self):
        return self.__requester_id

    # status
    @property
    def status(self):
        return self.__status

    # type
    @property
    def type(self):
        return self.__type

    # endregion


    # region Public Instance Methods
    def to_dict(self):
        return {
            'created': datetime_to_unix_timestamp(self.created),
            'id': self.id,
            'status': self.status,
            'type': int(self.type)
        }
    # endregion


    # region Public Class Methods
    @classmethod
    def from_bootnode_task(cls, bn_task):
        celery_task = AsyncResult(bn_task.id)
        return cls.from_components(celery_task, bn_task)

    @classmethod
    def from_components(cls, celery_task, bootnode_task):
        return cls(cls.__combine_components(celery_task, bootnode_task), celery_task, bootnode_task)

    @classmethod
    def from_id(cls, id_):
        bootnode_task = BnTask.objects.get(id=id_)
        celery_task = AsyncResult(id_)
        return cls.from_components(celery_task, bootnode_task)
    # endregion


    # region Private Static Methods
    @staticmethod
    def __combine_components(celery_task, bootnode_task):
        return {
            'created': bootnode_task.created,
            'id': celery_task.id,
            'requester_id': bootnode_task.requester_id,
            'status': celery_task.status,
            'type': bootnode_task.type
        }
    # endregion

# endregion
