# region Imports
from __future__ import absolute_import
# standard lib
from enum import IntEnum
# endregion


# region Public
class ImageCategory(IntEnum):
    os = 1
    application = 2
    snap = 3


class TaskType(IntEnum):
    build_node = 1
    attach_ip = 2
    power_off = 3
    power_on = 4
    suspend = 5
    unsuspend = 6
    delete_node = 7
    free_ip = 8
# endregion
