# region Imports
from __future__ import absolute_import
# standard lib
from copy import copy
# django
from operator import add
from django.conf import settings
# bootnode
from core.domain.security_rule import SecurityRule
from metadata.models import NodeConfiguration as BnNodeConfiguration
from openstack.flavors import get_flavor_by_name
# internal
from core.domain.changeset import ChangeSet
from core.domain.image import Image
from core.domain.security_flavor import SecurityFlavor
# endregion

# region Public
class NodeConfiguration(object):
    """A hardware and software configuration of which node copies can be spun up

    Public properties:
        property name                   type                    permissions
        -------------                   ----                    -----------
        bootnode_node_configuration     BnNodeConfiguration     read
        cpus                            int                     read/write
        disk                            int                     read/write
        has_public_ip                   bool                    read/write
        id                              int                     read
        image                           Image                   read/write
        image_id                        int                     read/write
        memory                          int                     read/write
        name                            str                     read/write
        security_flavors                list[SecurityFlavor]    read/write
    """


    # region Setup
    def __init__(self, fields, bn_node_configuration=None):
        self.__bn_node_configuration = bn_node_configuration

        self.__cpus = fields.get('cpus')
        self.__disk = fields.get('disk')
        self.__has_public_ip = fields.get('has_public_ip')
        self.__id = fields.get('id')
        self.__image = fields.get('image')
        self.__image_id = fields.get('image_id') if self.__image is None else self.__image.id
        self.__memory = fields.get('memory')
        self.__name = fields.get('name')
        self.__security_flavors = fields.get('security_flavors', [])

        self.__changeset = ChangeSet()

    # endregion


    # region Properties

    # bootnode_node_configuration
    @property
    def bootnode_node_configuration(self):
        return self.__bn_node_configuration

    # cpus
    @property
    def cpus(self):
        return self.__cpus

    @cpus.setter
    def cpus(self, value):
        self.__changeset.add('cpus', self.__cpus, value)
        self.__cpus = value

    # disk
    @property
    def disk(self):
        return self.__disk

    @disk.setter
    def disk(self, value):
        self.__changeset.add('disk', self.__disk, value)
        self.__disk = value

    # has_public_ip
    @property
    def has_public_ip(self):
        return self.__has_public_ip

    @has_public_ip.setter
    def has_public_ip(self, value):
        self.__changeset.add('has_public_ip', self.__has_public_ip, value)
        self.__has_public_ip = value

    # id
    @property
    def id(self):
        return self.__id

    # image
    @property
    def image(self):
        if self.__image is None and self.__image_id is not None:
            self.__image = Image.from_bootnode_image_id(settings.BOOTNODE_MASTER_CLOUD_HOST, self.__image_id)
        return self.__image

    @image.setter
    def image(self, value):
        self.__changeset.add('image', self.__image, value)
        self.__image = value
        self.__image_id = self.__image.id

    # image_id
    @property
    def image_id(self):
        if self.__image_id is None and self.__image is not None:
            self.__image_id = self.__image.id
        return self.__image_id

    @image_id.setter
    def image_id(self, value):
        self.__changeset.add('image_id', self.image_id, value)
        self.__image_id = value
        # null out the image and just lazy load
        self.__image = None

    # memory
    @property
    def memory(self):
        return self.__memory

    @memory.setter
    def memory(self, value):
        self.__changeset.add('memory', self.__memory, value)
        self.__memory = value

    # name
    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, value):
        self.__changeset.add('name', self.__name, value)
        self.__name = value

    # security_flavors
    @property
    def security_flavors(self):
        return copy(self.__security_flavors)

    # endregion


    # region Public Instance Methods
    def get_security_rules(self):
        """Get a list of all security rules, without security flavor hierarchy"""
        return reduce(add, map(lambda security_flavor: map(SecurityRule.as_dict, security_flavor.rules),
                               self.security_flavors))

    def to_dict(self):
        return {
            'cpus': self.cpus,
            'disk': self.disk,
            'has_public_ip': self.has_public_ip,
            'id': self.id,
            'image_id': self.image_id,
            'memory': self.memory,
            'name': self.name
        }
    # endregion


    # region Public Class Methods
    @classmethod
    def from_bn_node_configuration(cls, bn_node_configuration):
        return cls(NodeConfiguration.__dict_from_bn_node_configuration(bn_node_configuration), bn_node_configuration)

    @classmethod
    def from_id(cls, id_):
        bn_node_configuration = BnNodeConfiguration.objects.get(id=id_)
        return cls.from_bn_node_configuration(bn_node_configuration)

    # endregion


    # region Private Static Methods
    @staticmethod
    def __dict_from_bn_node_configuration(bn_node_configuration):
        return {
            'cpus': bn_node_configuration.flavor.cpus,
            'disk': bn_node_configuration.flavor.disk,
            'has_public_ip': bn_node_configuration.has_public_ip,
            'id': bn_node_configuration.id,
            'image_id': bn_node_configuration.image_id,
            'memory': bn_node_configuration.flavor.ram,
            'name': bn_node_configuration.name,
            'security_flavors': map(SecurityFlavor.from_bootnode_security_flavor,
                                    bn_node_configuration.security_flavors.all())
        }
        # endregion

# endregion
