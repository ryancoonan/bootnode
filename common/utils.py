# region Imports
from __future__ import absolute_import
# standard lib
from datetime import datetime
import random
import string
# external
from pytz import UTC
# endregion


# region Public
unix_epoch = datetime(1970, 1, 1, tzinfo=UTC)


def random_string(length):
    """Generate a random string of ASCII characters of the given length"""
    return ''.join([random.choice(string.ascii_letters) for _ in xrange(1, length)])


def descend_dict(d, keys):
    """Recursively descend a dictionary using the list of keys given"""
    sub = d
    for key in keys:
        if key in sub:
            sub = sub[key]
        else:
            return None
    return sub


def datetime_to_unix_timestamp(dt):
    """Convert a Python datetime to a Unix timestamp (ms)"""
    if dt is None:
        return None

    if dt.tzinfo != UTC:
        raise AttributeError('datetime must be UTC')

    return (dt - unix_epoch).total_seconds() * 1000


def model_dict(model, fields=None):
    """Convert a model into a dict

    :type fields: list[str]
    :type model: django.db.models.Model
    :rtype : dict[str, object]
    :param model:
    :param fields:
    :return:
    """
    if fields is not None:
        fields = frozenset(fields)
    return {
        field: getattr(model, field)
        for field
        in model._meta.get_all_field_names() if fields is None or field in fields
    }

def queryset_dict(qs, fields=None):
    """Convert a QuerySet into a list of dicts

    :type fields: list[str]
    :type qs: django.db.models.QuerySet
    :rtype : list[dict[str, object]]
    :param qs: QuerySet to be converted
    :param fields: List of fields to include in the dict
    :return: List of dicts
    """
    return [model_dict(model, fields) for model in qs]


def utc_now():
    """Get the current UTC time"""
    return datetime.utcnow().replace(tzinfo=UTC)
# endregion
