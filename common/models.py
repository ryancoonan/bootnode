# region Imports
from __future__ import absolute_import
# django
from django.db import models
# endregion


class BootnodeModel(models.Model):
    """Base model for (most) bootnode models to extend"""
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    deleted = models.DateTimeField(default=None, null=True, blank=True)

    class Meta:
        abstract = True
