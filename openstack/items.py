# region Imports
from __future__ import absolute_import
# internal
from openstack.exceptions import OpenStackIntegrityError
from openstack.tokens import make_request_as_admin
# endregion


# region Public Functions
def get_items(host_name, collection_url, collection_name, ids=None, names=None):
    """Get items, filtered by ID or name"""
    # TODO use Redis cache
    if ids is not None and names is not None:
        raise ValueError('cannot look up items by both IDs and names')

    items = make_request_as_admin(host_name, collection_url)[collection_name]
    if ids is not None:
        items = _filter_items(items, 'id', ids)
    if names is not None:
        items = _filter_items(items, 'name', names)
    return items


def get_singleton_item(host_name, collection_url, collection_name, name):
    """Get a named unique OpenStack item"""
    items = get_items(host_name, collection_url, collection_name, None, [name])
    if len(items) != 1:
        raise OpenStackIntegrityError('invalid number of {0} {1}: {2}'.format(name, collection_name, len(items)))

    return items[0]
# endregion


# region Private Functions
def _filter_items(items, field, set_):
    set_ = frozenset(set_)
    return filter(lambda item: item[field] in set_, items)
# endregion
