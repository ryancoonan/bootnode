# region Imports
from __future__ import absolute_import
# external
from requests import delete, post, put
# django
from django.conf import settings
# internal
from openstack import endpoints
from openstack.exceptions import NotFound, OpenStackIntegrityError
from openstack.tokens import make_request_as_admin
from openstack.items import get_items, get_singleton_item
# endregion


# region Defaults
# TODO named security group rule sets
default_security_group_rules = [
    {'port': '80', 'direction': 'ingress', 'protocol': 'tcp', 'ethertype': 'IPv4'},
    {'port': '22', 'direction': 'ingress', 'protocol': 'tcp', 'ethertype': 'IPv4'},
    {'port': '80', 'direction': 'egress', 'protocol': 'tcp', 'ethertype': 'IPv4'},
    {'port': '22', 'direction': 'egress', 'protocol': 'tcp', 'ethertype': 'IPv4'},
    {'port': '9090', 'direction': 'egress', 'protocol': 'tcp', 'ethertype': 'IPv4'},
    {'port': '9090', 'direction': 'ingress', 'protocol': 'tcp', 'ethertype': 'IPv4'},
    {'port': '4200', 'direction': 'egress', 'protocol': 'tcp', 'ethertype': 'IPv4'},
    {'port': '4200', 'direction': 'ingress', 'protocol': 'tcp', 'ethertype': 'IPv4'}
]
# endregion


# region Public Functions
def attach_floating_ip(host_name, fixed_ip_address):
    """Attach a floating IP address to the given fixed IP address"""
    url = endpoints.floating_ips(host_name)
    payload = _build_attach_floating_ip_payload(host_name, fixed_ip_address)
    return make_request_as_admin(host_name, url, payload=payload, method=post)['floatingip']


def create_security_group(host_name, name, rules=None):
    """Create security group, optionally with list of rules"""
    url = endpoints.security_groups(host_name)
    payload = {'security_group': {'name': name}}
    security_group = make_request_as_admin(host_name, url, payload=payload, method=post)['security_group']

    if rules is not None:
        create_security_group_rules(host_name, security_group['id'], rules)

    return security_group


def create_security_group_rule(host_name, security_group_id, rule):
    """Create a security group rule"""
    url = endpoints.security_group_rules(host_name)
    payload = _build_create_security_group_rule_payload(security_group_id, rule)
    make_request_as_admin(host_name, url, payload=payload, method=post)


def create_security_group_rules(host_name, security_group_id, rules):
    """Create a list of security group rules"""
    for rule in rules:
        create_security_group_rule(host_name, security_group_id, rule)


def delete_port(host_name, port_id):
    """Delete a port"""
    url = endpoints.ports(host_name, port_id=port_id)
    make_request_as_admin(host_name, url, method=delete)


def delete_security_group(host_name, security_group_id):
    """Delete a security group"""
    url = endpoints.security_groups(host_name, security_group_id=security_group_id)
    make_request_as_admin(host_name, url, method=delete)


def delete_security_group_by_name(host_name, security_group_name):
    """Delete a security group by its name"""
    url = endpoints.security_groups(host_name)
    delete_security_group(host_name, get_singleton_item(host_name, url, 'security_groups', security_group_name)['id'])


def free_floating_ip(host_name, floating_ip_id):
    """Free a floating IP"""
    url = endpoints.floating_ips(host_name, floating_ip_id=floating_ip_id)
    make_request_as_admin(host_name, url, method=delete)


def free_floating_ip_by_address(host_name, address):
    free_floating_ip(host_name, get_floating_ip_by_address(host_name, address)['id'])


def get_floating_ip_by_address(host_name, address):
    """Get a floating IP by its IP address"""
    payload = {'floating_ip_address': address}
    url = endpoints.floating_ips(host_name)
    floating_ips = make_request_as_admin(host_name, url, payload=payload, payload_format='querystring')['floatingips']
    if len(floating_ips) == 0:
        raise NotFound('no floating IP found with address {0}'.format(address))
    if len(floating_ips) > 1:
        raise OpenStackIntegrityError('multiple floating IPs found for address {0}'.format(address))

    return floating_ips[0]


def get_floating_ips(host_name):
    """Get floating IPs"""
    return get_items(host_name, endpoints.floating_ips(host_name), 'floatingips')


def get_networks(host_name, ids=None, names=None):
    """Get networks, optionally filtered by IDs or names"""
    return get_items(endpoints.networks(host_name), 'networks', ids, names)


def get_port_by_fixed_ip(host_name, fixed_ip_address):
    """Get a port by its associated fixed IP address"""
    for port in get_ports(host_name):
        if 'fixed_ips' in port:
            for fixed_ip in port['fixed_ips']:
                if fixed_ip.get('ip_address') == fixed_ip_address:
                    return port

    raise NotFound('could not find port corresponding to fixed IP address {0}'.format(fixed_ip_address))


def get_ports(host_name):
    """Get ports"""
    return get_items(host_name, endpoints.ports(host_name), 'ports')


def get_singleton_network(host_name, network_name):
    """Get a named unique network"""
    return get_singleton_item(host_name, endpoints.networks(host_name), 'networks',
                              settings.OPENSTACK_NETWORKS[network_name])


# endregion


# region Private Functions
def _build_attach_floating_ip_payload(host_name, fixed_ip_address):
    return {
        'floatingip': {
            'floating_network_id': get_singleton_network(host_name, 'public')['id'],
            'port_id': get_port_by_fixed_ip(host_name, fixed_ip_address)['id'],
            'fixed_ip_address': fixed_ip_address
        }
    }


def _build_create_security_group_rule_payload(security_group_id, rule):
    return {
        'security_group_rule': {
            'security_group_id': security_group_id,
            'direction': rule['direction'],
            'ethertype': rule['ethertype'],
            'protocol': rule['protocol'],
            'port_range_min': rule['port'],
            'port_range_max': rule['port']
        }
    }

# endregion
