# region Imports
from __future__ import absolute_import
# endregion


class InvalidEndpointOptions(Exception):
    """An invalid combination of options has been provided"""
    pass


class NotFound(Exception):
    """An expected item was not found"""
    pass


class OpenStackIntegrityError(Exception):
    """Data in OpenStack is in an invalid state"""
    pass


class OpenStackRequestException(Exception):
    """A request to the OpenStack backend returned a non-successful status code"""
    pass
