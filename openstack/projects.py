# region Imports
from __future__ import absolute_import
# django
from django.conf import settings
# internal
from openstack import endpoints
from openstack.items import get_items, get_singleton_item
# endregion


# region Public Functions
def get_admin_project_id(host_name):
    """Get the ID of the OpenStack admin project"""
    return get_admin_project(host_name)['id']


def get_admin_project(host_name):
    """Get the OpenStack admin project"""
    return get_singleton_item(host_name, endpoints.projects(host_name), 'projects', settings.OPENSTACK_ADMIN_PROJECT)


def get_projects(host_name, ids=None, names=None):
    """Get projects, optionally filtered by ID or name"""
    return get_items(endpoints.projects(host_name), 'projects', ids, names)
# endregion
