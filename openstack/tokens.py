# region Imports
from __future__ import absolute_import
# external
import requests
# django
from django.conf import settings
# internal
from openstack import endpoints
from openstack.http import make_request
# endregion


# region Public Functions
def get_project_token(host_name, project_name, username, password):
    """Get the access token for project_name using the provided project credentials"""
    # TODO use Redis cache
    response = _request_project_token(host_name, password, project_name, username)
    token, expires = _parse_project_token_response(response)
    return token


def get_admin_token(host_name):
    """Get the access token for the OpenStack admin"""
    return get_project_token(host_name, settings.OPENSTACK_ADMIN_PROJECT, settings.OPENSTACK_ADMIN_USERNAME,
                             settings.OPENSTACK_ADMIN_PASSWORD)


def make_request_as_admin(host_name, url, payload=None, method=requests.get, payload_format='body'):
    """Make a request to the OpenStack backend with admin authorization"""
    return make_request(url, payload=payload, token=get_admin_token(host_name), method=method,
                        payload_format=payload_format)


# endregion


# region Private Functions
def _create_project_token_request_payload(password, project_name, username):
    return {
        'auth': {
            'tenantName': project_name,
            'passwordCredentials': {
                'username': username,
                'password': password
            }
        }
    }


def _parse_project_token_response(response):
    return response['access']['token']['id'], response['access']['token']['expires']


def _request_project_token(host_name, password, project_name, username):
    payload = _create_project_token_request_payload(password, project_name, username)
    return make_request(endpoints.tokens(host_name), payload=payload, method=requests.post)

# endregion
