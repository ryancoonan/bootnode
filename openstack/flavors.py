# region Imports
from __future__ import absolute_import
# external
from requests import post
# internal
from openstack import endpoints
from openstack.exceptions import OpenStackIntegrityError
from openstack.tokens import make_request_as_admin
from openstack.items import get_items, get_singleton_item
from openstack.projects import get_admin_project_id
# endregion


# region Public Functions
def create_flavor(host_name, name, vcpus, disk, ram):
    """Create a flavor on the given host"""
    url = endpoints.flavors(host_name, get_admin_project_id(host_name))
    payload = _build_create_flavor_payload(host_name, name, vcpus, disk, ram)
    return make_request_as_admin(host_name, url, payload=payload, method=post)['flavor']


def get_flavors(host_name, ids=None, names=None):
    """Get flavors, optionally filtered by IDs or names"""
    url = endpoints.flavors(host_name, get_admin_project_id(host_name), detail=True)
    return get_items(host_name, url, 'flavors', ids, names)


def get_flavor_by_id(host_name, flavor_id):
    """Get a flavor by its ID"""
    url = endpoints.flavors(host_name, get_admin_project_id(host_name), flavor_id=flavor_id)
    return make_request_as_admin(host_name, url)['flavor']


def get_flavor_by_name(host_name, flavor_name):
    """Get a flavor by its name"""
    url = endpoints.flavors(host_name, get_admin_project_id(host_name), detail=True)
    return get_singleton_item(host_name, url, 'flavors', flavor_name)


def get_or_create_flavor_by_name(host_name, flavor_name):
    """The flavor name is actually the component specification of the flavor"""
    try:
        flavor = get_flavor_by_name(host_name, flavor_name)
    except OpenStackIntegrityError:
        vcpus, disk, ram = map(int, flavor_name.split('_'))
        flavor = create_flavor(host_name, flavor_name, vcpus, disk, ram)
    return flavor
# endregion


# region Private
def _build_create_flavor_payload(host_name, name, vcpus, disk, ram):
    return {
        'flavor': {
            'name': name,
            'ram': ram,
            'vcpus': vcpus,
            'disk': disk
        }
    }
# endregion
