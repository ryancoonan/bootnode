# region Imports
from __future__ import absolute_import
# standard lib
from base64 import b64encode
# external
from requests import post, delete
# internal
from openstack import endpoints
from openstack.exceptions import OpenStackRequestException
from openstack.tokens import make_request_as_admin
from openstack.items import get_items
from openstack.networking import get_singleton_network
from openstack.projects import get_admin_project_id
# endregion


# region Public Functions
def create_server(host_name, server_name, image_id, flavor_id, security_group_id, public_rsa_key=None, admin_pass=None):
    """Create a new server"""
    payload = _build_create_server_payload(host_name, admin_pass, flavor_id, image_id, public_rsa_key,
                                           security_group_id, server_name)

    url = endpoints.servers(host_name, get_admin_project_id(host_name))
    return make_request_as_admin(host_name, url, payload=payload, method=post)['server']


def delete_server(host_name, server_id):
    """Delete a server"""
    url = endpoints.servers(host_name, get_admin_project_id(host_name), server_id=server_id)
    return make_request_as_admin(host_name, url, method=delete)


def execute_server_action(host_name, server_id, action_name, payload_body=None):
    url = endpoints.servers(host_name, get_admin_project_id(host_name), server_id=server_id, action=True)
    return make_request_as_admin(host_name, url, payload={action_name: payload_body}, method=post)


def get_server(host_name, server_id):
    """Get a server"""
    url = endpoints.servers(host_name, get_admin_project_id(host_name), server_id=server_id, detail=False)
    try:
        return make_request_as_admin(host_name, url)['server']
    except OpenStackRequestException:
        return None


def get_servers(host_name, ids=None, names=None, detail=True):
    """Get servers, optionally filtered by IDs or names"""
    return get_items(endpoints.servers(host_name, get_admin_project_id(host_name), detail=detail), 'servers', ids,
                     names)


def get_vnc_url(host_name, server_id):
    """Request a VNC session and return the URL"""
    action_response = execute_server_action(host_name, server_id, 'os-getVNCConsole', payload_body={'type': 'novnc'})
    return action_response['console']['url']


def suspend_server(host_name, server_id):
    """Suspend a server"""
    return execute_server_action(host_name, server_id, 'suspend')


# endregion


# region Private Functions
def _build_create_server_payload(host_name, admin_pass, flavor_id, image_id, public_rsa_key, security_group_id,
                                 server_name):
    server = {
        'name': server_name,
        'imageRef': image_id,
        'flavorRef': flavor_id,
        'networks': [
            {
                'uuid': get_singleton_network(host_name, 'private')['id']
            }
        ],
        'security_groups': [{
            'id': security_group_id
        }]
    }
    if public_rsa_key is not None:
        server['personality'] = [{
            'path': '/root/.ssh/authorized_keys',
            'contents': b64encode(public_rsa_key + ' generated-by-bootnode-api')
        }]
    if admin_pass is not None:
        server['adminPass'] = admin_pass
    return {'server': server}

# endregion
