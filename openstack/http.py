# region Imports
from __future__ import absolute_import
# standard lib
import json
# external
import requests
# internal
from openstack.exceptions import OpenStackRequestException
# endregion


# region Public Functions
def make_request(url, payload=None, token=None, method=requests.get, payload_format='body'):
    """Make a request to the OpenStack backend"""
    headers = _prepare_headers(payload, token)
    response = _execute_request(headers, method, payload, payload_format, url)
    return _parse_response(response)
# endregion


# region Private Functions
def _execute_request(headers, method, payload, payload_format, url):
    if payload is not None:
        if payload_format == 'body':
            response = method(url, data=json.dumps(payload), headers=headers)
        elif payload_format == 'querystring':
            response = method(url, params=payload, headers=headers)
        else:
            raise ValueError('unrecognized payload format: {0}'.format(payload_format))
    else:
        response = method(url, headers=headers)
    return response


def _parse_response(response):
    if response.status_code >= 300:
        raise OpenStackRequestException('response.text: {0}'.format(response.text))

    return response.json() if response.text is not None and len(response.text) > 0 else None


def _prepare_headers(payload, token):
    headers = {}
    if payload is not None:
        headers['Content-Type'] = 'application/json'
    if token is not None:
        headers['X-Auth-Token'] = token
    return headers
# endregion
