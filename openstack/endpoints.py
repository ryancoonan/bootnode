# region Imports
from __future__ import absolute_import
# django
from django.conf import settings
# internal
from openstack.exceptions import InvalidEndpointOptions
# endregion

# region Base URLs
_base_keystone_v2_url = 'http://{0}:5000/v2.0'.format('{0}.' + settings.BOOTNODE_CLOUD_HOST)
_base_keystone_v3_url = 'http://{0}:5000/v3'.format('{0}.' + settings.BOOTNODE_CLOUD_HOST)
_base_nova_url = 'http://{0}:8774/v2'.format('{0}.' + settings.BOOTNODE_CLOUD_HOST)
_base_admin_url = 'http://{0}:35357/v2.0'.format('{0}.' + settings.BOOTNODE_CLOUD_HOST)
_base_neutron_url = 'http://{0}:9696/v2.0'.format('{0}.' + settings.BOOTNODE_CLOUD_HOST)
_base_glance_url = 'http://{0}:9292/v1'.format('{0}.' + settings.BOOTNODE_CLOUD_HOST)
# endregion


# region Public Functions
def project_user_roles(host_name, project_id, user_id, role_id):
    """Build a URL for getting or modifying user's project roles"""
    base_url = _base_keystone_v3_url.format(host_name)
    return '{0}/projects/{1}/users/{2}/roles/{3}'.format(base_url, project_id, user_id, role_id)


def tokens(host_name):
    """Build a URL for getting or modifying access tokens"""
    return '{0}/tokens'.format(_base_keystone_v2_url.format(host_name))


def servers(host_name, project_id, server_id=None, detail=False, action=False):
    """Build a URL for getting or modifying servers"""
    if detail and action:
        raise InvalidEndpointOptions('detail and action parameters are mutually exclusive')
    if action and server_id is None:
        raise InvalidEndpointOptions('server_id must be specified for action')

    base_url = _base_nova_url.format(host_name)
    url = '{0}/{1}/servers'.format(base_url, project_id)
    if server_id is not None:
        url += '/{0}'.format(server_id)
    if detail:
        url += '/detail'
    if action:
        url += '/action'
    return url


def users(host_name):
    """Build a URL for getting or modifying users"""
    return '{0}/users'.format(_base_keystone_v3_url.format(host_name))


def projects(host_name):
    """Build a URL for getting or modifying projects"""
    return '{0}/projects'.format(_base_keystone_v3_url.format(host_name))


def images(host_name, detail=False):
    """Build a URL for getting or modifying images"""
    url = '{0}/images'.format(_base_glance_url.format(host_name))
    if detail:
        url += '/detail'
    return url


def networks(host_name):
    """Build a URL for getting or modifying networks"""
    return '{0}/networks'.format(_base_neutron_url.format(host_name))


def subnets(host_name):
    """Build a URL for getting or modifying subnets"""
    return '{0}/subnets'.format(_base_neutron_url.format(host_name))


def routers(host_name, router_id=None, add_interface=False):
    """Build a URL for getting or modifying routers"""
    if add_interface and router_id is None:
        raise InvalidEndpointOptions('must specify router_id to add interface')

    url = '{0}/routers'.format(_base_neutron_url.format(host_name))
    if router_id is not None:
        url += '/{0}'.format(router_id)
    if add_interface:
        url += '/add_router_interface'
    return url


def ports(host_name, port_id=None):
    """Build a URL for getting or modifying ports"""
    url = '{0}/ports'.format(_base_neutron_url.format(host_name))
    if port_id is not None:
        url += '/{0}'.format(port_id)
    return url


def floating_ips(host_name, floating_ip_id=None):
    """Build a URL for getting or modifying floating IP addresses"""
    url = '{0}/floatingips'.format(_base_neutron_url.format(host_name))
    if floating_ip_id is not None:
        url += '/{0}'.format(floating_ip_id)
    return url


def security_groups(host_name, security_group_id=None):
    """Build a URL for getting or modifying security groups"""
    url = '{0}/security-groups'.format(_base_neutron_url.format(host_name))
    if security_group_id is not None:
        url += '/{0}'.format(security_group_id)
    return url


def security_group_rules(host_name):
    """Build a URL for getting or modifying security group rules"""
    return '{0}/security-group-rules'.format(_base_neutron_url.format(host_name))


def roles(host_name):
    """Build a URL for getting or modifying roles"""
    return '{0}/roles'.format(_base_keystone_v3_url.format(host_name))


def flavors(host_name, project_id, flavor_id=None, detail=False):
    """Build a URL for getting or modifying flavors"""
    url = '{0}/{1}/flavors'.format(_base_nova_url.format(host_name), project_id)
    if flavor_id is not None:
        url += '/{0}'.format(flavor_id)
    if detail:
        url += '/detail'
    return url
# endregion
