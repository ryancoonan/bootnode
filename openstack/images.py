# region Imports
from __future__ import absolute_import
# internal
from openstack import endpoints
from openstack.exceptions import OpenStackRequestException
from openstack.items import get_items, get_singleton_item
# endregion


# region Public Functions
def get_image_by_name(host_name, name):
    url = endpoints.images(host_name, detail=True)
    try:
        return get_singleton_item(host_name, url, 'images', name)
    except OpenStackRequestException:
        return None


def get_images(host_name, ids=None, names=None):
    """Get images, optionally filtered by IDs or names"""
    url = endpoints.images(host_name, detail=True)
    return get_items(url, 'images', ids, names)
# endregion
