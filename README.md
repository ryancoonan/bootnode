# bootnode | Web #

**Authors:** [Ryan Coonan](mailto:ryan@bootnode.com), [Tony Vattathil](mailto:tony@bootnode.com)

## Description ##
This project talks directly to OpenStack API and provides a simplified reseller UI on top of OpenStack. 

## Technologies ##
OpenStack 6.0/7.0+


### Frontend ###
**Languages:** [CoffeeScript (1.9.1)](http://coffeescript.org/), [Less (2.3.1)](http://lesscss.org/)

**Frameworks & Major Libraries:** [Backbone.js (1.1.2)](http://backbonejs.org/), [Underscore.js (1.8.2)](http://underscorejs.org/), [jQuery (2.1.3)](http://jquery.com/)

**Plugins & Minor Libraries:** [Lightbox_me (2.3)](http://buckwilson.me/lightboxme/), [jquery.cookie (1.4.1)](https://github.com/carhartl/jquery-cookie), [DateJS (1.0.0-rc3)](https://github.com/abritinthebay/datejs), [noVNC (0.4)](https://github.com/kanaka/noVNC)

### Backend ###
**Languages:** [Python (2.7.8)](https://www.python.org/)

**Frameworks & Major Libraries:** [Django (1.8.2)](https://www.djangoproject.com/), [Celery (3.1.18)](http://docs.celeryproject.org/en/latest/index.html)

**Plugins & Minor Libraries:** [Requests (2.7.0)](http://docs.python-requests.org/en/latest/), [Fabric (1.10.1)](http://www.fabfile.org/), [Flanker (0.4.32)](https://github.com/mailgun/flanker/blob/master/docs/User%20Manual.md)

**Web Server:** [Apache (2.4.6)](http://httpd.apache.org/), [mod_wsgi (3.4-12)](https://modwsgi.readthedocs.org/en/master/)

**Messaging Queue:** [RabbitMQ (3.3.5)](https://www.rabbitmq.com/)

**Cache:** [Redis (2.8.13)](http://redis.io/)

**Database:** [MariaDB (5.5.40)](https://mariadb.org/)

**Operating System:** [CentOS (7.0)](http://www.centos.org/)