# external
from django.conf.urls import include, url
from django.contrib import admin
admin.autodiscover()
# internal
import api.urls
import website.urls

urlpatterns = [
    url(r'^', include(website.urls)),
    url(r'^api/', include(api.urls)),
    url(r'^admin/', include(admin.site.urls))
]
