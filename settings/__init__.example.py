from __future__ import absolute_import
from .local import *

# secrets
SECRET_KEY = '<secret>'
DATABASES['default']['USER'] = '<username>'
DATABASES['default']['PASSWORD'] = '<password>'
OPENSTACK_ADMIN_PASSWORD = '<password>'
CELERY_RESULT_BACKEND = CELERY_RESULT_BACKEND_BASE.format(username='<username>', password='<password>')
