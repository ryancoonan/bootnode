"""
Django settings for bootnode project.
"""

from datetime import timedelta
import os
import os.path
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Application definition

INSTALLED_APPS = (
    'django.contrib.sites',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'common',
    'openstack',
    'metadata',
    'core',
    'api',
    'website',
    'setup'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'urls'

WSGI_APPLICATION = 'wsgi.application'

LOGIN_URL = '/welcome/'
APPEND_SLASH = True
SITE_ID = 1

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader'
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    "django.core.context_processors.request",
)

STATICFILES_DIRS = (
    'static',
)

ALLOWED_HOSTS = []

# celery config
BROKER_URL = 'amqp://guest:guest@localhost:5672//'
CELERY_RESULT_BACKEND_BASE = 'db+mysql://{username}:{password}@localhost/core'

CELERYBEAT_SCHEDULE = {
    'power-down-expired': {
        'task': 'core.tasks.free_locked_out_resources',
        'schedule': timedelta(hours=1)
    }
}

# openstack config
OPENSTACK_ADMIN_PROJECT = 'admin'
OPENSTACK_ADMIN_USERNAME = 'admin'
OPENSTACK_ADMIN_PASSWORD = None # __init__
OPENSTACK_NETWORKS = {
    'private': 'bn_private',
    'public': 'bn_public'
}
OPENSTACK_DATETIME_FORMAT = '%Y-%m-%dT%H:%M:%S.%f'

# bootnode config

# number of hours a user has to validate their email before account lockout
BOOTNODE_USER_VALIDATION_WINDOW = 24
