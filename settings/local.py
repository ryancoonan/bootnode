from __future__ import absolute_import
from .base import *

BOOTNODE_ENV = 'local'
BOOTNODE_CLOUD_HOST = 'bootnode.com'
BOOTNODE_MASTER_CLOUD_HOST = 'cloud2'

DEBUG = True
TEMPLATE_DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'core',
        'HOST': '127.0.0.1',
        'OPTIONS': {
            'charset': 'utf8mb4'
        }
    }
}
