from __future__ import absolute_import
from .base import *

# bootnode settings
BOOTNODE_ENV = 'staging'
BOOTNODE_CLOUD_HOST = 'bootnode.com'
BOOTNODE_MAIL_HOST = 'bootnode.com'
BOOTNODE_MAIL_DEFAULT_SENDER = 'hello@bootnode.com'
BOOTNODE_MAIL_WELCOME_BCCS = ['team@bootnode.com']
BOOTNODE_MASTER_CLOUD_HOST = 'cloud2'

DEBUG = True
TEMPLATE_DEBUG = True

ALLOWED_HOSTS += [
    'stage.bootnode.com'
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'core',
        'USER': 'root',
        'HOST': '127.0.0.1',
        'OPTIONS': {
        	'charset': 'utf8mb4'
        }
    }
}

EMAIL_HOST = 'mail.bootnode.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'hello'
EMAIL_HOST_PASSWORD = 'h3ll0n0d3'
EMAIL_USE_TLS = True

STATIC_ROOT = '/var/www/static/'

CSRF_COOKIE_SECURE = True
SESSION_COOKIE_SECURE = True
