import json
from fabric.api import *
import os, errno


# region Deployment Targets
@task
def stage():
    env.user = 'ryan'
    env.hosts = ['stage.bootnode.com']


@task
def prod():
    env.user = 'ryan'
    env.hosts = ['ssh.bootnode.com']
# endregion


# region Deploy
@task
def deploy():
    run('sudo chown -R ryan:dev /var/www/static')
    with cd('/var/www/bootnode'):
        with prefix('source env/bin/activate'):
            run('sudo systemctl stop httpd')
            run('git fetch origin')
            run('git pull')
            run('pip install -r requirements.txt')
            run('sudo chown -R ryan:dev website core api settings')
            run('fab build')
            run('python manage.py migrate --fake-initial')
            run('python manage.py collectstatic --clear --noinput -i env')
            run('fab remap_coffee')
            run('sudo systemctl start httpd')
# endregion


# region Host-local Tasks
@task
def build():
    _transpile_coffeescript()
    _transpile_less()


@task
def test():
    local('python manage.py test')


@task
def remap_coffee():
    for project in ['website', 'core']:
        _remap_coffee('/var/www/static/{0}'.format(project))


@task
def refresh():
    local('sudo chown -R ryan:dev /var/www/static')
    with cd('/var/www/bootnode'):
        with prefix('source env/bin/activate'):
            local('pip install -r requirements.txt')
            local('sudo chown -R ryan:dev website core api settings')
            build()
            local('python manage.py migrate --fake-initial')
            local('python manage.py collectstatic --clear --noinput -i env')
            remap_coffee()
            local('sudo systemctl restart httpd')
# endregion


# region Private Functions
def _remap_coffee(base_dir):
    for path, dirs, files in os.walk(base_dir):
        map(_remap_coffee, dirs)
        for file_ in files:
            if file_.endswith('.map'):
                with open(os.path.join(path, file_), 'r') as map_file:
                    map_props = json.loads(map_file.read())
                    map_props['sourceRoot'] = '/static'
                    map_props['sources'][0] = map_props['sources'][0].split('/static/', 1)[-1]
                with open(os.path.join(path, file_), 'w') as map_file:
                    map_file.write(json.dumps(map_props))


def _transpile_coffeescript():
    cmd_base = 'coffee --compile --output {project}/static/{project}/js/ {project}/static/{project}/coffee/'
    for project in ['core', 'website']:
        local(cmd_base.format(project=project))


def _transpile_less():
    dir_base = '{project}/static/{project}'
    cmd_base = 'lessc {dir}/less/{file}.less > {dir}/css/{file}.css'
    for project in ['website']:
        directory = dir_base.format(project=project)
        try:
            os.makedirs('{0}/css'.format(directory))
        except OSError as ex:
            if ex.errno == errno.EEXIST:
                pass
            else:
                raise
        for filename in os.listdir('{0}/less'.format(directory)):
            if '.' in filename:
                filename_no_extension, extension = filename.rsplit('.', 1)
                if extension == 'less':
                    cmd = cmd_base.format(dir=directory, file=filename_no_extension)
                    local(cmd)
# endregion
