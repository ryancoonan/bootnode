# external
from django.conf.urls import url
# internal
import views

urlpatterns = [
    url(r'^login/', views.login_),
    url(r'^logout/', views.logout_),
    url(r'^nodes/(?P<node_id>.+)/(?P<node_action>.+)$', views.nodes_action),
    url(r'^nodes/(?P<node_id>.+)$', views.nodes),
    url(r'^nodes/', views.nodes),
    url(r'^console/(.+)', views.console),
    url(r'^users/(\d+)', views.users),
    url(r'^users/', views.users),
    url(r'^keys/(?P<user_id>.+)$', views.user_key),
    url(r'^tasks/(?P<task_id>.+)$', views.tasks),
    url(r'^tasks/', views.tasks),
]
