# region Imports
from __future__ import absolute_import
# django
from django.http import JsonResponse
# endregion


# region Public
class ApiErrorResponse(JsonResponse):
    """Error response class"""

    def __init__(self, api_error, message, status=400, *args, **kwargs):
        content = {'error_code': api_error.value, 'message': message}
        super(ApiErrorResponse, self).__init__(content, status=status, *args, **kwargs)

# endregion
