# region Imports
from __future__ import absolute_import
# internal
from core.exceptions import BootnodeError
# endregion


# region Validators
def _validate_node_name(name):
    name_len = len(name)
    return 0 < name_len < 100, BootnodeError.invalid_node_name
# endregion


# region Private
class _RequestSpec(object):
    querystring = []
    post = []
    json_body = []


class _RequestSpecRule(object):
    def __init__(self, param, validator=None, required=False):
        self.param = param
        self.validator = validator
        self.required = required
# endregion


# region Public
class CreateNodeRequestSpec(_RequestSpec):
    """Validation rules for the create node endpoint"""
    json_body = [
        _RequestSpecRule('name', required=True, validator=_validate_node_name),
        _RequestSpecRule('image_name', required=True),
        _RequestSpecRule('flavor_name', required=True)
    ]


class CreateUserRequestSpec(_RequestSpec):
    """Validation rules for the create user endpoint"""
    post = [
        _RequestSpecRule('email', required=True),
        _RequestSpecRule('password', required=True)
    ]


class LoginRequestSpec(_RequestSpec):
    """Validation rules for the login endpoint"""
    post = [
        _RequestSpecRule('username', required=True),
        _RequestSpecRule('password', required=True)
    ]
# endregion
