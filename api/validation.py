# region Imports
from __future__ import absolute_import
# standard lib
import json
# django
from django.http import JsonResponse
# bootnode
from core.domain.user import User
from core.exceptions import BootnodeError
# internal
from api.errors import ApiErrorResponse
# endregion


# region Public
def no_locked_out_users(endpoint):
    """Decorator to check that a user is not locked out"""
    def no_locked_out_users_wrapper(*args, **kwargs):
        request = args[0]
        user = User.from_django_auth_user(request.user)
        if user.is_locked_out:
            return ApiErrorResponse(BootnodeError.user_is_locked_out, 'this user is locked out', status=403)
        else:
            return endpoint(*args, **kwargs)

    return no_locked_out_users_wrapper


def validate(request_spec):
    """Decorator for API endpoints to validate requests"""

    def validate_with_spec_wrapper(endpoint):
        def validate_with_spec(*args, **kwargs):
            request = args[0]
            valid, errors = validate_request(request, request_spec)
            if not valid:
                return JsonResponse({'errors': errors}, status=400)

            return endpoint(*args, **kwargs)

        return validate_with_spec

    return validate_with_spec_wrapper


def validate_request(request, request_spec):
    """Validate an API request according to its specified validation rules"""
    errors = _validate_request_parts(request, querystring=request_spec.querystring, post=request_spec.post,
                                     json_body=request_spec.json_body)
    return len(errors) == 0, errors

# endregion


# region Private
def _validate_request_part(request_part, rules):
    errors = []
    for rule in rules:
        request_arg = request_part.get(rule.param)
        errors += _validate_request_arg(request_arg, rule)

    return errors


def _validate_request_arg(request_arg, rule):
    errors = []
    if rule.required and request_arg is None:
        return [BootnodeError.missing_required_parameter]

    if request_arg is not None and rule.validator is not None:
        valid, error = rule.validator(request_arg)
        if not valid:
            errors.append(error)

    return errors


def _validate_request_parts(request, querystring=None, post=None, json_body=None):
    errors = []
    if querystring is not None and len(querystring) > 0:
        errors += _validate_request_part(request.GET, querystring)
    if post is not None and len(post) > 0:
        errors += _validate_request_part(request.POST, post)
    if json_body is not None and len(json_body) > 0:
        errors += _validate_request_part(json.loads(request.body), json_body)

    return errors

# endregion
