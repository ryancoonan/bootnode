# region Imports
from __future__ import absolute_import
# standard lib
import json
# django
from django.conf import settings
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponse
from django.shortcuts import redirect
# bootnode
from core.domain.node import Node
from core.domain.task import Task
from core.domain.user import User
from core.emails import send_register_email
from core.exceptions import BootnodeError, BootnodeException
from core.repository.nodes import get_nodes, create_node
from core.repository.tasks import get_tasks
# internal
from api import request_specs as specs
from api.errors import ApiErrorResponse
from api.validation import validate, no_locked_out_users
# endregion


# region Public Functions
@login_required
@no_locked_out_users
def console(request, node_id):
    """Initiate a VNC connection and return the URL"""
    node = Node.from_id(node_id)
    if node.owner.id != request.user.id:
        return ApiErrorResponse(BootnodeError.wrong_node_owner, 'user does not own this node')

    if node.status != 'ACTIVE':
        return ApiErrorResponse(BootnodeError.node_not_powered_on, 'this node is not powered on')

    console_url = node.get_console_url()
    if console_url is None:
        return ApiErrorResponse(BootnodeError.unknown, 'could not get console', status=500)

    return redirect(console_url)


@validate(specs.LoginRequestSpec)
def login_(request):
    """Log a user in using credentials"""
    user_ = User.from_credentials(request.POST['username'], request.POST['password'])
    if user_ is not None:
        login(request, user_.django_auth_user)
        return HttpResponse(request.GET.get('next', '/'))


@login_required
def logout_(request):
    """Log a user out"""
    logout(request)
    return redirect('/')


@login_required
@no_locked_out_users
def nodes(request, node_id=None):
    """RESTful endpoint for nodes"""
    if node_id is None:
        if request.method == 'GET':
            return _get_nodes(request)
        if request.method == 'POST':
            return _create_node(request)
    else:
        if request.method == 'GET':
            return _get_node(request, node_id)
        if request.method == 'DELETE':
            return _delete_node(request, node_id)

    return _method_unsupported(request.method, request.path)


@login_required
@no_locked_out_users
def nodes_action(request, node_id=None, node_action=None):
    if not node_id or not node_action:
        return ApiErrorResponse(BootnodeError.missing_required_parameter, 'must have node_id and node_action')

    try:
        node = Node.from_id(node_id)
        node.action(node_action)
    except BootnodeException as ex:
        if settings.DEBUG:
            raise
        return ApiErrorResponse(ex.error_code, 'could not take action', status=500)
    except Exception as ex:
        if settings.DEBUG:
            raise
        return ApiErrorResponse(BootnodeError.unknown, 'could not take action', status=500)

    return JsonResponse(node.to_dict())


@login_required
@no_locked_out_users
def tasks(request, task_id=None):
    if task_id is None:
        if request.method == 'GET':
            return _get_tasks(request)
    else:
        if request.method == 'GET':
            return _get_task(request, task_id)


@login_required
def user_key(request, user_id):
    if request.user.id != long(user_id):
        return ApiErrorResponse(BootnodeError.wrong_user, 'authenticated user does not match requested user', status=403)

    user_ = User.from_django_auth_user(request.user)
    return HttpResponse(user_.private_key)


def users(request, user_id=None):
    """RESTful endpoint for users"""
    if user_id is None and request.method == 'POST':
        return _create_user(request)

    return _method_unsupported(request.method, request.path)
# endregion


# region Private Functions
@login_required
@validate(specs.CreateNodeRequestSpec)
def _create_node(request):
    """Create a node"""
    body = json.loads(request.body)
    user = User.from_django_auth_user(request.user)
    name = body['name']
    image_name = body['image_name']
    flavor_name = body['flavor_name']

    try:
        node = create_node(user, name, image_name, flavor_name)
    except Exception as ex:
        # TODO catch specific errors
        if settings.DEBUG:
            raise
        return ApiErrorResponse(BootnodeError.unknown, 'could not create node', status=500)

    return JsonResponse(node.to_dict())


@validate(specs.CreateUserRequestSpec)
def _create_user(request):
    """Create a user"""
    email = request.POST['email']
    password = request.POST['password']
    try:
        user_ = User.register(email, password)
        send_register_email(user_)
    except Exception as ex:
        # TODO catch specific errors
        if settings.DEBUG:
            raise
        return ApiErrorResponse(BootnodeError.unknown, 'could not register user', status=500)

    login(request, authenticate(username=email, password=password))
    return HttpResponse(request.GET.get('next', '/'))


def _delete_node(request, node_id):
    """Delete a node"""
    node = Node.from_id(node_id)
    node.delete()
    return JsonResponse({})


def _get_node(request, node_id):
    """Get a node"""
    node = Node.from_id(node_id)
    if node.owner.id != request.user.id:
        return ApiErrorResponse(BootnodeError.wrong_node_owner, 'user does not own this node', status=403)

    return JsonResponse(node.to_dict())


def _get_nodes(request):
    """Get all nodes the authenticated user has access to"""
    try:
        nodes_ = get_nodes(request.user)
    except Exception as ex:
        # TODO catch specific errors
        if settings.DEBUG:
            raise
        return ApiErrorResponse(BootnodeError.unknown, 'could not get nodes', status=500)

    return JsonResponse(map(Node.to_dict, nodes_), safe=False)


def _get_task(request, task_id):
    task = Task.from_id(task_id)
    return JsonResponse(task.to_dict())


def _get_tasks(request):
    return JsonResponse(map(Task.to_dict, get_tasks(request.user)), safe=False)


def _method_unsupported(method, path):
    """Return an error for an unsupported REST call"""
    error_message = '{0} is not supported for endpoint: {1}'.format(method, path)
    return ApiErrorResponse(BootnodeError.method_not_supported, error_message, status=501)
# endregion
