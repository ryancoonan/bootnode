# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Application',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('deleted', models.DateTimeField(default=None, null=True, blank=True)),
                ('target', models.CharField(max_length=250, blank=True)),
                ('cta', models.CharField(max_length=250, blank=True)),
                ('name', models.CharField(max_length=90, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('deleted', models.DateTimeField(default=None, null=True, blank=True)),
                ('title', models.CharField(max_length=200)),
                ('body', models.TextField()),
                ('pub_date', models.DateTimeField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Flavor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('deleted', models.DateTimeField(default=None, null=True, blank=True)),
                ('name', models.CharField(max_length=100)),
                ('disk', models.IntegerField()),
                ('cpus', models.IntegerField()),
                ('ram', models.IntegerField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Host',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('deleted', models.DateTimeField(default=None, null=True, blank=True)),
                ('name', models.CharField(max_length=50)),
                ('region', models.CharField(max_length=30, blank=True)),
                ('datacenter', models.CharField(max_length=30, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('deleted', models.DateTimeField(default=None, null=True, blank=True)),
                ('display_name', models.CharField(max_length=100)),
                ('name', models.CharField(max_length=30)),
                ('distro', models.CharField(max_length=30)),
                ('version', models.CharField(max_length=30)),
                ('user_created', models.BooleanField(default=False)),
                ('icon', models.CharField(max_length=40)),
                ('category', models.IntegerField()),
                ('application', models.ForeignKey(blank=True, to='metadata.Application', null=True)),
                ('creator', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('doc_page', models.ForeignKey(to='metadata.Document')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Keyword',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('deleted', models.DateTimeField(default=None, null=True, blank=True)),
                ('keyword_text', models.CharField(max_length=100)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Kit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('deleted', models.DateTimeField(default=None, null=True, blank=True)),
                ('host', models.ForeignKey(blank=True, to='metadata.Host', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='KitConfiguration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('deleted', models.DateTimeField(default=None, null=True, blank=True)),
                ('name', models.CharField(max_length=100)),
                ('landing_layout', models.TextField()),
                ('link_postfix', models.CharField(max_length=20)),
                ('application', models.ForeignKey(blank=True, to='metadata.Application', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Node',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('deleted', models.DateTimeField(default=None, null=True, blank=True)),
                ('server_id', models.CharField(max_length=50, null=True, blank=True)),
                ('name', models.CharField(max_length=100)),
                ('security_group_id', models.CharField(max_length=50, null=True, blank=True)),
                ('task', models.CharField(max_length=50, null=True, blank=True)),
                ('flavor', models.ForeignKey(to='metadata.Flavor')),
                ('host', models.ForeignKey(to='metadata.Host', null=True)),
                ('image', models.ForeignKey(to='metadata.Image')),
                ('kit', models.ForeignKey(blank=True, to='metadata.Kit', null=True)),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='NodeConfiguration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('deleted', models.DateTimeField(default=None, null=True, blank=True)),
                ('name', models.CharField(max_length=100)),
                ('has_public_ip', models.BooleanField(default=False)),
                ('flavor', models.ForeignKey(to='metadata.Flavor')),
                ('image', models.ForeignKey(to='metadata.Image')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='SecurityFlavor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('deleted', models.DateTimeField(default=None, null=True, blank=True)),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='SecurityRule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('deleted', models.DateTimeField(default=None, null=True, blank=True)),
                ('port', models.IntegerField()),
                ('direction', models.CharField(max_length=10)),
                ('protocol', models.CharField(max_length=10)),
                ('ethertype', models.CharField(max_length=10)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='SshKey',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('deleted', models.DateTimeField(default=None, null=True, blank=True)),
                ('public_key', models.TextField()),
                ('name', models.CharField(max_length=50)),
                ('is_default', models.BooleanField(default=False)),
                ('securely_generated', models.BooleanField(default=False)),
                ('private_key', models.TextField(default=None, null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('deleted', models.DateTimeField(default=None, null=True, blank=True)),
                ('id', models.CharField(max_length=190, serialize=False, primary_key=True)),
                ('type', models.IntegerField()),
                ('requester', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='UserDetail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('deleted', models.DateTimeField(default=None, null=True, blank=True)),
                ('console_password', models.CharField(max_length=10)),
                ('allowed_nodes', models.IntegerField(default=3)),
                ('allowed_ips', models.IntegerField(default=1)),
                ('home_host', models.ForeignKey(blank=True, to='metadata.Host', null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='securityflavor',
            name='rules',
            field=models.ManyToManyField(to='metadata.SecurityRule'),
        ),
        migrations.AddField(
            model_name='nodeconfiguration',
            name='security_flavors',
            field=models.ManyToManyField(to='metadata.SecurityFlavor'),
        ),
        migrations.AddField(
            model_name='node',
            name='ssh_key',
            field=models.ForeignKey(to='metadata.SshKey'),
        ),
        migrations.AddField(
            model_name='kitconfiguration',
            name='default_node',
            field=models.ForeignKey(related_name='primary_to', to='metadata.NodeConfiguration'),
        ),
        migrations.AddField(
            model_name='kitconfiguration',
            name='node_configurations',
            field=models.ManyToManyField(to='metadata.NodeConfiguration'),
        ),
        migrations.AddField(
            model_name='kit',
            name='kit_configuration',
            field=models.ForeignKey(to='metadata.KitConfiguration'),
        ),
        migrations.AddField(
            model_name='kit',
            name='owner',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='image',
            name='keywords',
            field=models.ManyToManyField(to='metadata.Keyword', blank=True),
        ),
        migrations.AddField(
            model_name='document',
            name='keywords',
            field=models.ManyToManyField(to='metadata.Keyword'),
        ),
        migrations.AddField(
            model_name='document',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
