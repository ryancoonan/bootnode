# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('metadata', '0002_node_source_configuration'),
    ]

    operations = [
        migrations.AddField(
            model_name='userdetail',
            name='email_confirmation_code',
            field=models.CharField(max_length=30, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='userdetail',
            name='email_confirmed',
            field=models.BooleanField(default=False),
        ),
    ]
