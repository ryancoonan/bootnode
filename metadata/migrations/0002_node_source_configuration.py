# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('metadata', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='node',
            name='source_configuration',
            field=models.ForeignKey(blank=True, to='metadata.NodeConfiguration', null=True),
        ),
    ]
