# region Imports
from __future__ import absolute_import
# standard lib
import os
# external
from Crypto.PublicKey import RSA
# django
from django.contrib.auth.models import User
from django.db import models
# bootnode
from common.models import BootnodeModel
from common.utils import random_string
# endregion


# region Public
class Keyword(BootnodeModel):
    """Keywords for mapping to Documents and Images"""
    keyword_text = models.CharField(max_length=100)

    def __unicode__(self):
        return self.keyword_text


class Document(BootnodeModel):
    """HTML Documents to show alongside Images or Kits"""
    user = models.ForeignKey(User)
    title = models.CharField(max_length=200)
    body = models.TextField()
    pub_date = models.DateTimeField()
    keywords = models.ManyToManyField(Keyword)

    def __unicode__(self):
        return self.title


class Application(BootnodeModel):
    """Specification for an application on a node"""
    target = models.CharField(max_length=250, blank=True)
    cta = models.CharField(max_length=250, blank=True)
    name = models.CharField(max_length=90, blank=True)

    def __unicode__(self):
        return self.name


class Image(BootnodeModel):
    """OpenStack Images metadata"""
    display_name = models.CharField(max_length=100)
    name = models.CharField(max_length=30)
    distro = models.CharField(max_length=30)
    version = models.CharField(max_length=30)
    user_created = models.BooleanField(default=False, blank=True)
    creator = models.ForeignKey(User, null=True, blank=True)
    doc_page = models.ForeignKey(Document)
    keywords = models.ManyToManyField(Keyword, blank=True)
    icon = models.CharField(max_length=40)
    category = models.IntegerField()
    application = models.ForeignKey(Application, blank=True, null=True)

    def __unicode__(self):
        return self.display_name


class Flavor(BootnodeModel):
    """OpenStack Flavors metadata"""
    name = models.CharField(max_length=100)
    disk = models.IntegerField()
    cpus = models.IntegerField()
    ram = models.IntegerField()

    def __unicode__(self):
        return self.name

    @classmethod
    def get_or_create_from_configuration(cls, cpus, disk, ram):
        try:
            flavor_ = cls.objects.get(cpus=cpus, disk=disk, ram=ram)
        except cls.DoesNotExist:
            name = '{0}_{1}_{2}'.format(cpus, disk, ram)
            flavor_ = Flavor.objects.create(cpus=cpus, disk=disk, ram=ram, name=name)
            flavor_.save()
        return flavor_


class SshKey(BootnodeModel):
    """SSH keys for user accounts. Currently only one key for an account"""
    public_key = models.TextField()
    user = models.ForeignKey(User)
    name = models.CharField(max_length=50)
    is_default = models.BooleanField(default=False)
    securely_generated = models.BooleanField(default=False)
    private_key = models.TextField(null=True, default=None)

    def __unicode__(self):
        return '[{0}] {1}'.format(self.user.email, self.name)

    @staticmethod
    def create(user):
        key_pair = RSA.generate(2048, os.urandom)
        key = SshKey()
        key.public_key = key_pair.publickey().exportKey('OpenSSH')
        key.user = user
        key.is_default = True
        key.private_key = key_pair.exportKey('PEM')
        return key


class Host(BootnodeModel):
    """An OpenStack controller"""

    name = models.CharField(max_length=50)
    region = models.CharField(max_length=30, blank=True)
    datacenter = models.CharField(max_length=30, blank=True)

    def __unicode__(self):
        return '[{0}] [{1}] {2}'.format(self.region, self.datacenter, self.name)


class SecurityRule(BootnodeModel):
    """An individual firewall rule"""

    port = models.IntegerField()
    direction = models.CharField(max_length=10)
    protocol = models.CharField(max_length=10)
    ethertype = models.CharField(max_length=10)

    def __unicode__(self):
        return '{0} ({1})'.format(self.port, self.direction)


class SecurityFlavor(BootnodeModel):
    """Security rule set"""

    name = models.CharField(max_length=100)
    rules = models.ManyToManyField(SecurityRule)

    def __unicode__(self):
        return self.name


class NodeConfiguration(BootnodeModel):
    """A configuration specification for generating a single node"""

    flavor = models.ForeignKey(Flavor)
    image = models.ForeignKey(Image)
    name = models.CharField(max_length=100)
    security_flavors = models.ManyToManyField(SecurityFlavor)
    has_public_ip = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name


class KitConfiguration(BootnodeModel):
    """A specification for a full kit environment - what nodes to build, and how to lay out the page"""

    default_node = models.ForeignKey(NodeConfiguration, related_name='primary_to')
    name = models.CharField(max_length=100)
    node_configurations = models.ManyToManyField(NodeConfiguration)
    landing_layout = models.TextField()
    link_postfix = models.CharField(max_length=20)
    application = models.ForeignKey(Application, blank=True, null=True)

    def __unicode__(self):
        return self.name

    @staticmethod
    def create(name, node_configurations, landing_layout=None):
        kit = KitConfiguration()
        kit.name = name
        for node_configuration in node_configurations:
            kit.node_configurations.add(node_configuration)
        if landing_layout is not None:
            kit.landing_layout = landing_layout
        kit.link_postfix = random_string(20)
        return kit


class Kit(BootnodeModel):
    """An instance of a Kit"""

    owner = models.ForeignKey(User)
    kit_configuration = models.ForeignKey(KitConfiguration)
    host = models.ForeignKey(Host, blank=True, null=True)

    def __unicode__(self):
        return u'[{0}][{1}] {2}'.format(self.host, self.owner, self.kit_configuration)

class Node(BootnodeModel):
    """OpenStack Server metadata"""

    server_id = models.CharField(max_length=50, blank=True, null=True)
    owner = models.ForeignKey(User)
    name = models.CharField(max_length=100)
    image = models.ForeignKey(Image)
    security_group_id = models.CharField(max_length=50, blank=True, null=True)
    ssh_key = models.ForeignKey(SshKey)
    flavor = models.ForeignKey(Flavor)
    host = models.ForeignKey(Host, null=True)
    kit = models.ForeignKey(Kit, blank=True, null=True)
    task = models.CharField(max_length=50, blank=True, null=True)
    source_configuration = models.ForeignKey(NodeConfiguration, blank=True, null=True)

    def __unicode__(self):
        return u'[{0}] {1}'.format(self.owner, self.name)

    @classmethod
    def create_(cls, server_id, owner, name, image_name, flavor_name, security_group_id, host_id, save=False):
        node = cls()
        node.server_id = server_id
        node.owner = owner
        node.name = name
        node.host_id = host_id
        node.image = Image.objects.get(name=image_name, deleted=None)
        node.flavor = Flavor.objects.get(name=flavor_name, deleted=None)
        node.security_group_id = security_group_id
        node.ssh_key = SshKey.objects.get(user=owner, deleted=None)
        if save:
            node.save()
        return node


class UserDetail(BootnodeModel):
    """Extended metadata for a User account

    See: https://docs.djangoproject.com/en/1.8/topics/auth/customizing/#specifying-a-custom-user-model
    """

    user = models.ForeignKey(User)
    console_password = models.CharField(max_length=10)
    allowed_nodes = models.IntegerField(default=3)
    allowed_ips = models.IntegerField(default=1)
    home_host = models.ForeignKey(Host, blank=True, null=True)
    email_confirmation_code = models.CharField(max_length=30, blank=True, null=True)
    email_confirmed = models.BooleanField(default=False)
    lockout_at = models.DateTimeField(default=None, blank=True, null=True)

    def __unicode__(self):
        return unicode(self.user)

class Task(BootnodeModel):
    """Extended metadata for a celery task"""

    id = models.CharField(max_length=190, primary_key=True)
    requester = models.ForeignKey(User)
    type = models.IntegerField()

    def __unicode__(self):
        return '[{0}] Type {1}, requested by {2} at {3}'.format(self.id, self.type, self.requester.email, self.created)
# endregion
