/*jslint white: false */
/*global window, $, Util, RFB, */
"use strict";

var rfb;

window.setCanvasSize = function(rfb, scale) {
    if (!scale || typeof(scale) !== typeof(0.1)) {
        scale = 1.03;
    }
    if (!rfb) {
        rfb = window.rfb;
    }
    var display = rfb.get_display();
    display.set_scale(scale);
}

window.onload = function() {
    var host, port, password, path, token;

    // By default, use the host and port of server that served this file
    host = WebUtil.getQueryVar('host', window.location.hostname);
    port = WebUtil.getQueryVar('port', window.location.port);

    // If a token variable is passed in, set the parameter in a cookie.
    // This is used by nova-novncproxy.
    token = WebUtil.getQueryVar('token', null);
    if (token) {
        WebUtil.createCookie('token', token, 1)
    }

    path = WebUtil.getQueryVar('path', 'websockify');

    rfb = new RFB({
        'target': $D('noVNC_canvas'),
        'encrypt': WebUtil.getQueryVar('encrypt', (window.location.protocol === "https:")),
        'repeaterID': WebUtil.getQueryVar('repeaterID', ''),
        'true_color': WebUtil.getQueryVar('true_color', true),
        'local_cursor': WebUtil.getQueryVar('cursor', true),
        'shared': WebUtil.getQueryVar('shared', true),
        'view_only': WebUtil.getQueryVar('view_only', false),
        'updateState': window.setCanvasSize,
        'onPasswordRequired': function() {
            return;
        },
        'viewport': true
    });
    rfb.connect(host, port, password, path);

    window.rfb = rfb;

    $('#noVNC_canvas').attr('tabindex', '0')
        .mousedown(function () { 
            $(this).focus();
            return false;
        });
};
