# region Imports
from __future__ import absolute_import
# standard lib
from enum import IntEnum
# django
from django.utils import timezone
# bootnode
from common.utils import queryset_dict, model_dict
# internal
from website.models import Toast, ShowToastTo
# endregion


# region Public
class ToastType(IntEnum):
    info = 1
    lockout_timer = 2
    locked_out = 3
    info_show_once = 4


def get_toasts(user):
    if user.email_confirmed:
        return _get_info_toasts(user)
    if user.lockout_at >= timezone.now():
        return _get_lockout_timer_toasts()
    else:
        return _get_locked_out_toasts()

# endregion


# region Private
_client_fields = ['id', 'priority', 'text', 'type', 'name']

def _get_locked_out_toasts():
    locked_out_toasts = Toast.objects.filter(deleted=None, expires__gte=timezone.now(),
                                             type=ToastType.locked_out)
    return queryset_dict(locked_out_toasts, fields=_client_fields)


def _get_lockout_timer_toasts():
    lockout_toasts = Toast.objects.filter(deleted=None, type=ToastType.lockout_timer)
    return queryset_dict(lockout_toasts, fields=_client_fields)


def _get_info_toasts(user):
    plain_info_toasts = _get_plain_info_toasts()
    show_once_toasts = _get_show_once_toasts(user)
    return plain_info_toasts + show_once_toasts


def _get_plain_info_toasts():
    info_toasts = Toast.objects.filter(deleted=None, expires__gte=timezone.now(), type=int(ToastType.info))
    return queryset_dict(info_toasts, fields=_client_fields)


def _get_show_once_toasts(user):
    show_toast_tos = ShowToastTo.objects.filter(deleted=None, user=user.django_auth_user, shown=False)
    if len(show_toast_tos) == 0:
        return []

    show_once_toasts = map(lambda toast_to: toast_to.toast, show_toast_tos)
    _mark_shown_once_toasts(user, show_toast_tos, show_once_toasts)
    return [model_dict(toast, fields=_client_fields) for toast in show_once_toasts]


def _mark_shown_once_toasts(user, show_toast_tos, show_once_toasts):
    top_toast = max(show_once_toasts, key=lambda toast: toast.priority)
    top_show_toast_to = show_toast_tos.get(toast=top_toast)
    top_show_toast_to.shown = True
    top_show_toast_to.save()

# endregion
