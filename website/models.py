# region Imports
from __future__ import absolute_import
# django
from django.db import models
from django.contrib.auth.models import User
# bootnode
from common.models import BootnodeModel
# endregion


# region Public
class Toast(BootnodeModel):
    """Toasts to display on the top bar of the page"""
    text = models.CharField(max_length=200)
    priority = models.IntegerField(default=0)
    expires = models.DateTimeField(blank=True, null=True)
    type = models.IntegerField(default=1)
    name = models.CharField(max_length=30, blank=True, null=True)

    def __unicode__(self):
        return '[{0}] {1}'.format(self.name, self.text) if self.name else self.text


class ShowToastTo(BootnodeModel):
    """Toasts of type info_show_once that have been shown to a user"""
    user = models.ForeignKey(User)
    toast = models.ForeignKey(Toast)
    shown = models.BooleanField(default=False)
# endregion
