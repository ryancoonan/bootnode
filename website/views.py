# region Imports
from __future__ import absolute_import
# standard lib
import json
# django
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import password_reset, password_reset_confirm
from django.shortcuts import render, redirect
from django.views.decorators.csrf import ensure_csrf_cookie
# bootnode
from core.domain.image import Image
from core.domain.kit import Kit
from core.domain.kit_configuration import KitConfiguration
from core.domain.node import Node
from core.domain.node_configuration import NodeConfiguration
from core.domain.task import Task
from core.domain.user import User
from core.repository.images import get_images, get_flavors_
from core.repository.kits import build_kit, get_kits
from core.repository.kit_configurations import get_kit_configurations
from core.repository.nodes import get_nodes
from core.repository.node_configurations import get_node_configurations
from core.repository.tasks import get_tasks
# internal
from website.models import ShowToastTo, Toast
from website.repository.toasts import get_toasts
# endregion


# region Endpoints
@login_required
def console(request):
    return render(request, 'website/console.html')


@ensure_csrf_cookie
@login_required
def home(request):
    return render(request, 'website/home.html', _bootstrap_website_data(request.user))


@login_required(login_url='/register/')
def kit(request, kit_postfix):
    user = User.from_django_auth_user(request.user)
    kit_configuration = KitConfiguration.from_link_postfix(kit_postfix)
    kit_request = build_kit(user, kit_configuration)
    return redirect('/')


@ensure_csrf_cookie
def register(request):
    return render(request, 'website/register.html')


@ensure_csrf_cookie
def reset_password(request):
    return password_reset(request, template_name='website/reset_password.html',
                          email_template_name='website/emails/reset_email.html',
                          subject_template_name='website/emails/reset_subject.txt', post_reset_redirect='/')


@ensure_csrf_cookie
def reset_password_confirm(request, uidb64, token):
    return password_reset_confirm(request, template_name='website/reset_confirm.html',
                                  uidb64=uidb64, token=token, post_reset_redirect='/')

def validate(request, code):
    user = User.from_validation_code(code)
    user.email_confirmed = True
    user.save()

    show_toast_to = ShowToastTo.objects.create(user=user.django_auth_user,
                                               toast=Toast.objects.get(name='email-validated'))
    show_toast_to.save()
    return redirect('/')

@ensure_csrf_cookie
def welcome(request):
    return render(request, 'website/login.html')
# endregion


# region Private Functions
def _bootstrap_website_data(django_auth_user):
    user = User.from_django_auth_user(django_auth_user)
    initial_data = {
        'nodeConfigurations': map(NodeConfiguration.to_dict, get_node_configurations()),
        'kitConfigurations': map(KitConfiguration.to_dict, get_kit_configurations()),
        'user': user.to_dict(),
        'nodes': map(Node.to_dict, get_nodes(django_auth_user)),
        'images': map(Image.to_dict, get_images(django_auth_user)),
        'flavors': get_flavors_(),
        'toasts': get_toasts(user),
        'tasks': map(Task.to_dict, get_tasks(django_auth_user)),
        'kits': map(Kit.to_dict, get_kits(django_auth_user))
    }
    return {key: json.dumps(value) for key, value in initial_data.iteritems()}
# endregion
