submitSearch = ->
  term = $('#term').val()
  $('#results').html('')
  $.ajax({
    method: 'GET',
    url: "/search/#{term}/",
    success: (data, textStatus, jqXHR) ->
      $.each(JSON.parse(data).documents, (_, document) ->
        docLink = "<li><a href=\"/show/#{document.title}\">" +
            "#{document.title}</a></li>"
        $('#results').append docLink)
  })

$ -> $('#somebutton').on('click', submitSearch)
