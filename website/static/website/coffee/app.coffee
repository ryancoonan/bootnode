app = window.app

$ ->
  app.loadContext()

  app.router = new app.Router()
  app.router.start()
