app = window.app

class app.Task extends Backbone.Model
  urlRoot: '/api/tasks/'

  initialize: () ->
    @pollStatus()

  pollStatus: () =>
    if @get('status') isnt 'SUCCESS'
      setTimeout((() => @fetch({success: @pollStatus})), 2500)
