app = window.app

class app.Resource extends Backbone.Model
  defaults:
    used: null
    available: null

  initialize: (attributes, options) ->
    @getUsed = options.def.getUsed
    @getAvailable = options.def.getAvailable
    @user = options.user

    @update()

  update: ->
    @set('used', @getUsed())
    @set('available', (@getAvailable(@user)))
