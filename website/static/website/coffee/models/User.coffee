app = window.app

class app.User extends Backbone.Model
  urlRoot: '/api/user/'

  resourceDefs:
    nodes:
      getUsed: () -> app.nodes.length
      getAvailable: (user) -> user.get('allowed_nodes')
    publicIps:
      getUsed: () -> (app.nodes.filter((node) -> node.has('public_ipv4'))).length
      getAvailable: (user) -> user.get('allowed_ips')

  initialize: () -> @setResources()

  setResources: () -> @set('resources', new app.Resources(null, {
    defs: @resourceDefs
    user: @
  }))

  hasResource: (resource) ->
    return @resourceDefs[resource].getUsed() < @resourceDefs[resource].getAvailable(@)

  getAccountActions: () ->
    actions = [{
      text: 'Logout'
      className: 'action-logout'
    }]
    return actions
