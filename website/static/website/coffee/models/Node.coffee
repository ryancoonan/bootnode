app = window.app

class app.Node extends Backbone.Model
  urlRoot: '/api/nodes/'

  initialize: () ->
    if @has('task_id')
      task = app.tasks.get(@get('task_id'))
      @updateTaskStatus(task)
      task.on('change', @updateTaskStatus)
    if @has('kit_id')
      @set('kit', app.kits.get(@get('kit_id')))
    @updateImageInfo()
    @on('change:image_name', @updateImageInfo)
    @updateFlavorInfo()
    @on('change:flavor_name', @updateFlavorInfo)
    @on('change:status', @pollStatus)

    @pollStatus()

  parse: (response) =>
    if @isTransitioning()
      if response.status is @transitionFrom
        response.status = @get('status')
    return response

  updateTaskStatus: (model) =>
    @set('taskStatus', model.get('status'))

  getDetails: () =>
    @fetch({success: @pollStatus})

  pollStatus: () =>
    if @isTransitioning()
      setTimeout(@getDetails, 5000)

  updateImageInfo: () => @set('image', app.images.findWhere({
    name: @get('image_name')}))

  updateFlavorInfo: () => @set('flavor', app.flavors.findWhere({
    name: @get('flavor_name')}))

  transitionStates:
    ACTIVE_POWERING_OFF: 0
    ACTIVE_SUSPENDING: 0
    SUSPENDED_UNSUSPENDING: 0
    SHUTOFF_POWERING_ON: 0
    BUILD: 0
    REBOOT: 0

  isTransitioning: () => @get('status') of @transitionStates
  transitionFrom: null

  actions:
    powerOff: (node) -> node.get('status') is 'ACTIVE'
    powerOn: (node) -> node.get('status') is 'SHUTOFF'
    getPublicIP: (node) ->
      not node.has('public_ipv4') and
      app.user.hasResource('publicIps') and
      node.has('private_ipv4')
    freePublicIP: (node) -> node.has('public_ipv4')
    downloadPrivateKey: (node) -> node.has('public_ipv4')
    popoutConsole: (node) -> node.get('status') is 'ACTIVE'
    suspend: (node) -> node.get('status') is 'ACTIVE'
    unsuspend: (node) -> node.get('status') is 'SUSPENDED'
    deleteNode: (node) -> node.get('status') isnt 'BUILD'

  submitAction: (action, success, error) ->
    $.ajax({
      url: "/api/nodes/#{@id}/#{action}"
      success: success
      error: error
    })

  powerOff: (success, error) ->
    if @actions.powerOff(@)
      @transitionFrom = @get('status')
      @set('status', 'ACTIVE_POWERING_OFF')
      @submitAction('power-off', success, (args...) =>
        @set('status', @transitionFrom)
        error?(args...))
      return true
    else
      return false

  powerOn: (success, error) ->
    if @actions.powerOn(@)
      @transitionFrom = @get('status')
      @set('status', 'SHUTOFF_POWERING_ON')
      @submitAction('power-on', success, (args...) =>
        @set('status', @transitionFrom)
        error?(args...))
      return true
    else
      return false

  getPublicIP: (success, error) ->
    if @actions.getPublicIP(@)
      $.ajax({
        url: "/api/nodes/#{@id}/get-ip"
        error: error
        success: (model, args...) =>
          @set('public_ipv4', model.public_ipv4)
          success?(ip, args...)
      })
      return true
    else
      return false

  freePublicIP: (success, error) ->
    if @actions.freePublicIP(@)
      $.ajax({
        url: "/api/nodes/#{@id}/free-ip"
        error: error
        success: (args...) =>
          @unset('public_ipv4')
          success?(args...)
      })
      return true
    else
      return false

  popoutConsole: () ->
    if @actions.popoutConsole(@)
      window.open("/api/console/#{@id}", '_blank')
#      $.ajax({
#        url: "/api/console/#{@id}"
#        async: false
#        success: (data) ->
#          urls = JSON.parse(data)
#          window.open(urls.cloud_url, '_blank')
#      })
    else
      return false

  suspend: (success, error) ->
    if @actions.suspend(@)
      @transitionFrom = @get('status')
      @set('status', 'ACTIVE_SUSPENDING')
      @submitAction('suspend', success, (args...) =>
        @set('status', @transitionFrom)
        error?(args...))
      return true
    else
      return false

  unsuspend: (success, error) ->
    if @actions.unsuspend(@)
      @transitionFrom = @get('status')
      @set('status', 'SUSPENDED_UNSUSPENDING')
      @submitAction('unsuspend', success, (args...) =>
        @set('status', @transitionFrom)
        error?(args...))
      return true
    else
      return false

  getActions: () -> _.filter(_.keys(@actions), (action) => @actions[action](@))
