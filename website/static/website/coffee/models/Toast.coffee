app = window.app

class app.Toast extends Backbone.Model
  urlRoot: '/api/toasts/'

  getText: () ->
    if @get('type') is app.types.toasts.lockout_timer
      timeLeft = @getTimeLeft()
      hours = Math.floor(timeLeft / (60*60))
      minutes = Math.floor((timeLeft % (60*60)) / 60)
      seconds = (timeLeft % (60*60)) % 60
      return @get('text').format(hours, minutes.leftZeroPad(2), seconds.leftZeroPad(2))
    else
      return @get('text')

  # returns time until lockout in seconds
  getTimeLeft: () ->
    return Math.round(((new Date(app.user.get('lockout_at'))).getTime() - Date.now()) / 1000)
