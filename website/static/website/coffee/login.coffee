submitLogin = () ->
  $promptContainer = $('#prompt-container')
  $loginButtonText = $('#login-button-text')

  if $promptContainer.hasClass('error')
    window.transitionText($promptContainer, 'Login',
      $promptContainer.removeClass.bind($promptContainer, 'error'))

  email = $('#email').val()
  password = $('#password').val()

  window.transitionText($loginButtonText, 'Logging in...')
  postUrl = '/api/login/'
  queryString = window.location.href.split('?', 2)[1]
  if queryString?
    postUrl += '?{0}'.format(queryString)
  $.ajax(postUrl, {
    method: 'POST'
    data:
      username: email
      password: password
    success: (url) ->
      window.location.href = url
    error: () ->
      window.transitionText($promptContainer,
        'Failed to login - username or password is incorrect',
        $promptContainer.addClass.bind($promptContainer, 'error'))
      window.transitionText($loginButtonText, 'Submit')
  })

$('#submit-login').on('click', submitLogin)
$('.submit-on-enter').keypress((event) ->
  if event.which is 13
    submitLogin())
