register = () ->
  $promptContainer = $('#prompt-container')
  $regButtonText = $('#submit-registration-text')

  if $promptContainer.hasClass('error')
    window.transitionText($promptContainer, 'Create your account',
      $promptContainer.removeClass.bind($promptContainer, 'error'))

  email = $('#email').val()
  password = $('#password').val()
  confirmPassword = $('#password-confirm').val()
  registrationCode = $('#registration-code').val()
  fail = false

  if password isnt confirmPassword
    msg = 'Passwords do not match'
    fail = true

  if not fail and password.length < 8 or password.length > 30
    msg = 'Password must be between 8 and 30 characters'
    fail = true

  unless fail
    emailParts = email.split('@')
    if emailParts.length isnt 2 or
        emailParts[0].length is 0 or
        emailParts[1].length is 0 or
        emailParts[1].split('.').length < 2
      msg = 'This does not look like a valid email'
      fail = true

  if fail
    window.transitionText($promptContainer, msg,
      $promptContainer.addClass.bind($promptContainer, 'error'))
    $('#submit-registration').one('click', register)
    $('.submit-on-enter').keypress(onKeypress)
    return

  window.transitionText($regButtonText, 'Building environment...')
  postUrl = '/api/users/'
  queryString = window.location.href.split('?', 2)[1]
  if queryString?
    postUrl += '?{0}'.format(queryString)

  $.ajax(postUrl, {
    method: 'POST'
    data:
      'email': email
      'password': password
      'registration_code': registrationCode
    success: (url) ->
      window.location.href = url
    error: () ->
      window.transitionText($promptContainer,
        'Something went wrong trying to create your account...',
        $promptContainer.addClass.bind($promptContainer, 'error'))
      window.transitionText($regButtonText, 'Sign Up')
  })

onKeypress = (event) ->
  if event.which is 13
    $('.submit-on-enter').unbind('keypress')
    register()

$('#submit-registration').one('click', register)
$('.submit-on-enter').keypress(onKeypress)
