app = window.app

app.loadContext = (contextData) ->
  contextData ?= app.context

  # TODO ship enums to the client
  app.types =
    toasts:
      info: 1
      lockout_timer: 2
      locked_out: 3
      info_show_once: 4

  app.nodeConfigurations = new app.NodeConfigurations(contextData.nodeConfigurations)
  app.kitConfigurations = new app.KitConfigurations(contextData.kitConfigurations)
  app.tasks = new app.Tasks(contextData.tasks)
  app.images = new app.Images(contextData.images)
  app.toasts = new app.Toasts(contextData.toasts)
  app.flavors = new app.Flavors(contextData.flavors)
  app.kits = new app.Kits(contextData.kits)
  app.nodes = new app.Nodes(contextData.nodes)
  app.user = new app.User(contextData.user)

  if app.context?
    delete app.context
