app = window.app

class app.AppView extends Backbone.View

  # templates
  appTemplate: _.template($('#app-template').html())
  nodeInfoTemplate: _.template($('#node-info-template').html())
  toastWrapperTemplate: _.template($('#toast-wrapper-template').html())
  accountInfoWrapperTemplate: _.template($('#account-info-wrapper-template').html())
  nodeActionsTemplate: _.template($('#node-actions-template').html())
  docsTemplate: _.template($('#docs-template').html())
  newNodeTemplate: _.template($('#new-node-template').html())
  nodePlaceholderTemplate: _.template($('#node-placeholder-template').html())

  ### SETUP ###

  minNodeEntries: 3
  previousDoc: null
  currentDoc: null
  creatingNode: false
  firstRender: true

  events:
    'click .placeholder-entry-wrapper': 'newNode'
    'click .new-node-image-entry': 'selectImage'
    'click .new-node-cancel-button': 'cancelNewNode'
    'click .new-node-create-button': 'createNode'
    'click .action-power-off': 'powerOff'
    'click .action-power-on': 'powerOn'
    'click .action-get-public-ip': 'getPublicIP'
    'click .action-free-public-ip': 'freePublicIP'
    'click .action-popout-console': 'popoutConsole'
    'click .action-suspend': 'suspend'
    'click .action-unsuspend': 'unsuspend'
    'click .action-delete': 'deleteNode'
    'click .action-logout': 'logout'
    'click .toast-close': 'toastClose'

  # initialize entry point for the page
  initialize: () ->
    @initializeSubViews()
    @wireListeners()
    if 'kit' of app
      @initializeKitLoad()

    @renderAll()

  initializeKitLoad: () ->
    kitLoadView = new app.KitBuildView(app.kit, {tasks: app.tasks})
    $(kitLoadView.render().el).lightbox_me({centered: true})

  wireListeners: () ->
    @listenTo(app.nodes, 'add', @nodeAdded)
    @listenTo(app.toasts, 'remove', @renderToast)

    @wireSelectedNodeListeners()

  # initialize the sub-views
  initializeSubViews: () ->
    @initializeNodeViews()

  # initialize a NodeView for each Node
  initializeNodeViews: () ->
    app.nodes.each(@createNodeView)
    app.nodes.selectDefault()?.view.selected = true

  ### RENDERING ###

  # render the entire page. typically only for initial page load
  renderAll: () ->
    @renderApp()
    @renderToast()
    @renderNodePanels()
    @renderAccountInfo()

  # render the top-level page layout
  renderApp: () -> @$el.html(@appTemplate({is_locked_out: app.user.get('is_locked_out')}))

  # render the toast at the top
  renderToast: () =>
    @currentToast = app.toasts.getTopToast()
    @$toastWrapper = @$('#toast-wrapper')
    if @currentToast?
      @$toastWrapper.html(@toastWrapperTemplate({
        toastText: @currentToast.getText()
      }))
      toastType = @currentToast.get('type')
      if toastType is app.types.toasts.info_show_once and @currentToast.get('name') is 'email-validated'
        @$toastWrapper.addClass('toast-wrapper-success')
      else if (toastType is app.types.toasts.lockout_timer or
               toastType is app.types.toasts.locked_out)
        @$toastWrapper.addClass('toast-wrapper-alert')
      else
        @$toastWrapper.addClass('toast-wrapper-info')

      if toastType is app.types.toasts.lockout_timer
        @startToastTimer()
    else
      @$toastWrapper.remove()

  startToastTimer: () ->
    setTimeout(@reRenderToast, 1000)

  reRenderToast: () =>
    if @currentToast?
      @$toastWrapper.html(@toastWrapperTemplate({
        toastText: @currentToast.getText()
      }))
      @startToastTimer()

  # render the left node info panel
  renderNodeInfo: () ->
    @$nodeInfo = @$('.node-info')
    if app.nodes.length > 0
      @$nodeInfo.html(@nodeInfoTemplate {
        nodeInfo: @getSelectedNode()?.view.getNodeInfoDisplay()
        consolePass: app.user.get('console_password')
      })
    else
      @$nodeInfo.html('')

  # render the list of nodes
  renderNodesList: () =>
  # sort in this order: (selected, real nodes, placeholders)
    nodeViewsFull = _.sortBy(@fillInNodesList(), (nodeView) ->
      unless nodeView?
        return 2
      if nodeView.selected
        return 0
      return 1)

    @$nodesList = @$('#nodes-list')
    quicksandSwapout = @$('#quicksandSwapout')
    if @firstRender
      targetEl = @$nodesList
    else
      targetEl = quicksandSwapout
      quicksandSwapout.html('')

    placeholderCount = 0
    _.each(nodeViewsFull, (nodeView) =>
      if nodeView?
        targetEl.append(nodeView.render().el)
      else
        targetEl.append(@nodePlaceholderTemplate({placeholderCount: placeholderCount++})))

    if @firstRender
      @wireNodeEntryListeners()
      @firstRender = false
    else
      @$nodesList.quicksand(@$('#quicksandSwapout li'), {
        adjustHeight: false,
        adjustWidth: false
      }, @wireNodeEntryListeners)

  # render the action controls for the selected node
  renderNodeActions: () =>
    @$nodeActions = @$('.node-actions')
    if app.nodes.length > 0
      selectedNode = @getSelectedNode()
      if selectedNode?
        @$nodeActions.html(@nodeActionsTemplate({
          nodeActions: selectedNode.view.getNodeActions()
          nodeID: selectedNode.id
        }))
    else
      @$nodeActions.html('')

# render the account info in top right
  renderAccountInfo: () ->
    @$accountInfoWrapper = @$('.account-info-wrapper')
    @$accountInfoWrapper.html(@accountInfoWrapperTemplate({user: app.user}))

  renderSelectedNodePanels: () ->
    @renderNodeInfo()
    @renderNodeActions()
    @renderDocPanel()

  renderNodePanels: () =>
    @renderNodesList()
    @renderSelectedNodePanels()

  renderDocPanel: () ->
    selectedNode = @getSelectedNode()
    if selectedNode?
      docId = selectedNode.get('image')?.get('default_doc_id')
      if docId? and (not @currentDocId? or @currentDocId isnt docId)
        @currentDocId = docId
        @transitionBigBoxContents(@docsTemplate({docId: docId}))
    else
      @newNode()


  ### FUNCTIONALITY ###

  toastClose: () =>
    $toastWrapper = @$('.toast-wrapper')
    $toastWrapper.html('')
    @currentToast = null
    app.toasts.closed()

  wireNodeEntryListeners: () =>
    @$('.node-entry').on('click', @nodeClicked)

  createNodeView: (node) =>
    node.view = new app.NodeView({model: node})
    @listenTo(node.view, 'selected', @nodeSelected)
    @listenTo(node.view, 'renderSelected', @renderSelectedNodePanels)

  nodeClicked: (event) ->
    nodeViewID = $(event.currentTarget).attr('data-id')
    app.nodes.find((node) -> node.view.id is nodeViewID)?.view?.setSelected(true)

  nodeAdded: (node) =>
    @createNodeView(node)
    @renderNodesList()
    # not 100% sure if we always select on add
    node.view.setSelected(true)

  getSelectedNode: () ->
    unless app.nodes.length > 0
      return null

    selectedFilter = app.nodes.filter((node) -> node.view.selected)
    if selectedFilter.length > 0
      return selectedFilter[0]
    else
      return null

  nodeSelected: (nodeView) ->
    previousSelectedNode = @findPreviousSelectedNodeView(nodeView)
    if previousSelectedNode?
      @deselectPreviousNode(previousSelectedNode)
    @listenTo(nodeView.model, 'change', @renderSelectedNodePanels)
    @renderNodePanels()

  findPreviousSelectedNodeView: (newSelection) ->
    previousSelectedFilter = app.nodes.filter((node) ->
      node.view.selected and node.id isnt newSelection.model.id)
    if previousSelectedFilter.length > 0
      return previousSelectedFilter[0].view
    else
      return null

  deselectPreviousNode: (previousNodeView) ->
    previousNodeView.setSelected false
    @stopListening(previousNodeView.model)

  nodeSelectClick: (event) =>
    unless @creatingNode
      id = event.currentTarget.id
      node = app.nodes.get id
      @selectNewNode(node)

  selectNewNode: (node) ->
    @getSelectedNode()?.view.setSelected(false)

    if node?
      @wireSelectedNodeListeners()
      node.view.setSelected(true)

    @renderNodePanels()

  reselectNode: () =>
    newNode = app.nodes.selectDefault()
    @selectNewNode(newNode)

  nodeRemoved: (node) =>
    if node.id is @getSelectedNode()?.id
      @reselectNode()
    else
      @renderNodesList()

  wireSelectedNodeListeners: () ->
    if @getSelectedNode()?
      @listenTo(@getSelectedNode(), 'change:status', @renderSelectedNodePanels)
      @listenTo(app.nodes, 'remove', @nodeRemoved)

  # fill in empty node views to inform the template to draw placeholders
  fillInNodesList: () ->
    nodeViewsFull = app.nodes.map((node) -> node.view)
    while nodeViewsFull.length < @minNodeEntries
      nodeViewsFull.push(null)
    return nodeViewsFull

  transitionBigBoxContents: (newContent,
                             callback = null,
                             savePrevious = true) ->
    @$docsWrapper = @$('.docs-wrapper')
    if @currentDoc?
      if savePrevious
        @previousDoc = @currentDoc
      else
        @previousDoc = null
      @currentDoc = newContent
      @$docsWrapper.animate({opacity: 0}, 750, 'swing', () =>
        @$docsWrapper.html(@currentDoc)
        @$docsWrapper.animate({opacity: 100}, 750)
        callback?())
    else
      @currentDoc = newContent
      @$docsWrapper.html(@currentDoc)
      callback?()

  newNode: () =>
    if not @creatingNode and app.user.hasResource('nodes')
      @creatingNode = true
      @transitionBigBoxContents(@newNodeTemplate({images: app.images}), () =>
        $('#new-node-name').keypress((event) =>
          keyCode = event.keyCode
          keyCode ?= event.which
          if keyCode is 13
            @createNode()))


  deselectImage: () ->
    @$("##{@selectedImage.get('id')}").removeClass('selected')
    @selectedImage = null

  selectImage: (event) =>
    if @selectedImage?
      @deselectImage()

    id = event.currentTarget.id
    image = app.images.get(id)
    @selectedImage = image
    @$(event.currentTarget).addClass('selected')

  cancelNewNode: () =>
    @creatingNode = false
    @transitionBigBoxContents(@previousDoc, null, false)

  validateName: (name) ->
    ret = [true, null]
    if name.length is 0
      ret = [false, 'Name cannot be empty']
    app.nodes.each((node) ->
      if name is node.get('name')
        ret = [false, 'There is already a node by that name'])
    return ret

  createNode: () =>
    name = @$('#new-node-name').val()
    [valid, msg] = @validateName(name)
    if !valid
      alert(msg)
      return

    imageName = @selectedImage.get('name')
    flavorName = app.flavors.first().get('name')

    app.nodes.create({
      name: name
      image_name: imageName
      flavor_name: flavorName
    })

    @creatingNode = false
    @transitionBigBoxContents(@docsTemplate({
      docId: app.images.findWhere({name: imageName}).get('default_doc_id')
    }))

  powerOn: () => @getSelectedNode()?.powerOn()
  powerOff: () => @getSelectedNode()?.powerOff()
  getPublicIP: () => @getSelectedNode()?.getPublicIP(null, () ->
    alert('Could not allocate public IP. We may be out of public IPs for now. Please check back later.'))
  freePublicIP: () => @getSelectedNode()?.freePublicIP()
  popoutConsole: () => @getSelectedNode()?.popoutConsole()
  suspend: () => @getSelectedNode()?.suspend()
  unsuspend: () => @getSelectedNode()?.unsuspend()
  deleteNode: () => @getSelectedNode()?.destroy()

  logout: () ->
    window.location = '/api/logout/'
