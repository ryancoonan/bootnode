app = window.app

prettyStatus =
  ACTIVE: 'Active'
  SUSPENDED: 'Suspended'
  SHELVED_OFFLOADED: 'Powered Off'
  BUILD: 'Building...'
  REBOOT: 'Rebooting...'
  SHUTOFF: 'Powered Off'
  ACTIVE_POWERING_OFF: 'Powering Off...'
  SHUTOFF_POWERING_ON: 'Powering On...'
  ACTIVE_SUSPENDING: 'Suspending...'
  SUSPENDED_UNSUSPENDING: 'Unsuspending...'

class app.NodeView extends Backbone.View

  # dom properties
  tagName: 'li'
  className: 'node-entry'

  # templates
  nodeTemplate: _.template($('#node-entry-template').html())

  # the fields to display along with display names and value formats
  fieldDisplay:
    private_ipv4:
      name: 'Private IP'
      get: (model) -> model.get('private_ipv4')
    public_ipv4:
      name: 'Public IP'
    disk:
      name: 'Disk'
      valueFormat: (value) -> "#{value} GB"
      get: (model) -> model.get('disk')
    memory:
      name: 'Memory'
      valueFormat: (value) -> "#{value} MB"
      get: (model) -> model.get('memory')
    vcpus:
      name: 'vCPUs'
      get: (model) -> model.get('cpus')
    image:
      name: 'Base Image'
      get: (model) -> model.get('image')?.get('display_name')
    created:
      name: 'Created'
      valueFormat: (value) -> (new Date(value)).toString('d')
    status:
      name: 'Status'
      prettyDict: prettyStatus
    taskStatus:
      name: 'Task'
      get: (model) -> model.get('taskStatus')
    kit:
      name: 'Kit'
      get: (model) ->
        if model.has('kit_id')
          kit = app.kits.get(model.get('kit_id'))
          return app.kitConfigurations.get(kit.get('configuration_id'))?.get('name')
        else
          return null

  initialize: () ->
    @id = @model.cid
    @$el.attr('data-id', @id)
    @wireListeners()

  events:
    'click': 'selectNode'

  selectNode: () => @setSelected(true)

  wireListeners: () ->
    @listenTo(@model, 'change', @reRender)

  reRender: () =>
    if @selected
      @trigger('renderSelected')
    else
      @render()

  render: () =>
    @$el.html(@nodeTemplate({node: @model}))
    @delegateEvents()
    return @

  getNodeInfoDisplay: () =>
    evaluated = _.map(@fieldDisplay, (valueArgs, field) =>
      if valueArgs.get?
        value = valueArgs.get(@model)
      else
        value = @model.get(field)
      if value?
        if valueArgs.valueFormat?
          value = valueArgs.valueFormat(value)
        else if valueArgs.prettyDict? and value of valueArgs.prettyDict
          value = valueArgs.prettyDict[value]
      return {field: valueArgs.name, value: value})
    return _.filter(evaluated, (pair) -> pair.value?)

  actions:
    powerOff:
      group: 'power'
      displayName: 'Power Off'
      className: 'action-power-off'
      opposite: 'powerOn'
      iconUri: 'website/images/actions/power.png'
    powerOn:
      group: 'power'
      displayName: 'Power On'
      className: 'action-power-on'
      opposite: 'powerOff'
      iconUri: 'website/images/actions/power.png'
    getPublicIP:
      group: 'modify'
      displayName: 'Get Public IP'
      className: 'action-get-public-ip'
      opposite: 'freePublicIP'
      iconUri: 'website/images/actions/publicip.png'
    freePublicIP:
      group: 'modify'
      displayName: 'Release Public IP'
      className: 'action-free-public-ip'
      opposite: 'getPublicIP'
      iconUri: 'website/images/actions/publicip.png'
    downloadPrivateKey:
      group: 'access'
      displayName: 'Download Private Key'
      className: 'action-download-private-key'
      iconUri: 'website/images/actions/key.png'
      href: () -> "/api/keys/#{app.user.get('id')}"
      download: 'cloud.key'
    popoutConsole:
      group: 'access'
      displayName: 'Popout Console'
      className: 'action-popout-console'
      iconUri: 'website/images/actions/fullscreen.png'
    suspend:
      group: 'power'
      displayName: 'Suspend'
      className: 'action-suspend'
      opposite: 'unsuspend'
      iconUri: 'website/images/actions/pause.png'
    unsuspend:
      group: 'power'
      displayName: 'Unsuspend'
      className: 'action-unsuspend'
      opposite: 'suspend'
      iconUri: 'website/images/actions/pause.png'
    deleteNode:
      group: 'modify'
      displayName: 'Delete'
      className: 'action-delete'
      iconUri: 'website/images/actions/delete.png'

  groupOrder: ['power', 'modify', 'access']

  getNodeActions: () =>
    actions = @model.getActions()
    sortedActions = _.sortBy(actions, (action) =>
      _.indexOf(@groupOrder, @actions[action].group))
    return _.map(sortedActions, (action) => @actions[action])

  setSelected: (selected) ->
    @selected = selected
    if selected
      @trigger('selected', @)
