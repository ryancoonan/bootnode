app = window.app

class app.KitBuildView extends Backbone.View
  template: _.template($('#kit-load-overlay-template').html())

  statuses:
    building:
      icon: 'progress_bar.gif'
      text: 'Kit is building...'
    done:
      icon: 'progress_bar.gif'
      text: 'Done!'

  buildSteps: [
    {
      checkState: () ->
        if app.nodes.length > 0 and
            app.nodes.first().get('status') is 'ACTIVE'
          return {
            icon: 'checkmark.png'
            text: 'Node built'
          }
        else
          return {
            icon: 'loading.gif'
            text: 'Building node'
          }
    },
    {
      checkState: () ->
        if app.nodes.length is 0 or
            app.nodes.first().get('status') isnt 'ACTIVE'
          return {
            icon: 'pending.png'
            text: 'IP waiting for node'
          }
        else if app.nodes.first().has('public_ipv4')
          return {
            icon: 'checkmark.png'
            text: 'Public IP allocated'
          }
        else
          return {
            icon: 'loading.gif'
            text: 'Allocating public IP'
          }
    }
  ]

  initialize: (model, options) ->
    @status = 'building'
    @tasks = options.tasks
    @setupStatusListeners()

  setupStatusListeners: () ->
    if @status is 'building'
      $.ajax({
        url: '/api/nodes'
        success: @statusCallback
      })

  statusCallback: (nodes) =>
    app.nodes = new app.Nodes(nodes)
    if _.every(@buildSteps, (step) -> step.checkState().icon is 'checkmark.png')
      @status = 'done'
    @render()
#    if @status is 'done'
#      setTimeout((-> window.location = '/'), 3000)
#    else
    @setupStatusListeners()

  render: () =>
    @$el.html(@template({
      statusIcon: @statuses[@status].icon
      statusText: @statuses[@status].text
      buildSteps: @buildSteps
      publicIp: if @status is 'done' then app.nodes.first().get('public_ipv4') else null
    }))
    return @

  makeBuildStep: (task) ->
    return {
      icon: 'add.png'
      text: 'Filler text'
    }
