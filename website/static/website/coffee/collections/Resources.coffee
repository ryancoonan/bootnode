app = window.app

class app.Resources extends Backbone.Collection
  model: app.Resource

  initialize: (models, options) ->
    @parseDefs(options)

  updateResources: () ->
    @each((resource) -> resource.update())

  parseDefs: (options) ->
    _.each(options.defs, (def, name) =>
      @add(new app.Resource({name: name}, {def: def, user: options.user})))
