app = window.app

class app.Nodes extends Backbone.Collection
  model: app.Node

  selectDefault: () ->
    return @first()
