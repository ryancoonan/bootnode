app = window.app

class app.Toasts extends Backbone.Collection
  model: app.Toast

  getTopToast: () ->
    @removeDismissedToasts()
    if @length > 0
      @selected = @max((toast) -> toast.get('priority'))
      # we never show more than one toast on a page load,
      # so after giving top toast, remove all others
      @removeAllOthers(@selected)
      return @selected
    else
      return null

  removeDismissedToasts: () ->
    _.each(@getDismissedToasts(), (toastID) =>
      toast = @find((toast) -> toast.id is toastID)
      if toast?
        @remove toast)

  removeAllOthers: (keepToast) ->
    removeToasts = @filter((toast) -> toast.id isnt keepToast.id)
    _.each(removeToasts, (toast) => @remove toast)

  closed: () ->
    @recordDismissedToast(@selected.id)
    @remove(@selected)
    @selected = null

  recordDismissedToast: (toastID) ->
    dismissedToasts = @getDismissedToasts()
    dismissedToasts.push(toastID)
    localStorage.setItem('dismissedToasts', dismissedToasts.join())

  getDismissedToasts: () ->
    dismissedToastsString = localStorage.getItem('dismissedToasts')
    if dismissedToastsString?
      return _.map(dismissedToastsString.split(','), parseInt)
    else
      return []
