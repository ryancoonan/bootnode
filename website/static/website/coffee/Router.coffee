app = window.app

class app.Router extends Backbone.Router
  routes:
    '': 'index'

  initialize: () ->
    @$appBody = $('#app-body')

  index: () ->
    appView = new app.AppView()
    @$appBody.html(appView.render().el)

  start: () ->
    Backbone.history.start()
