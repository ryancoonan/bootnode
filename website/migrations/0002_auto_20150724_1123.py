# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('website', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ShowToastTo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('deleted', models.DateTimeField(default=None, null=True, blank=True)),
                ('shown', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='toast',
            name='name',
            field=models.CharField(max_length=30, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='toast',
            name='type',
            field=models.IntegerField(default=1),
        ),
        migrations.AlterField(
            model_name='toast',
            name='expires',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='showtoastto',
            name='toast',
            field=models.ForeignKey(to='website.Toast'),
        ),
        migrations.AddField(
            model_name='showtoastto',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
