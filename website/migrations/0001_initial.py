# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Toast',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('deleted', models.DateTimeField(default=None, null=True, blank=True)),
                ('text', models.CharField(max_length=200)),
                ('priority', models.IntegerField(default=0)),
                ('expires', models.DateTimeField()),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
