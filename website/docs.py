# external
from django.http import HttpResponse
# internal
from metadata.models import Document


def show(request, doc_id):
    document = Document.objects.get(id=doc_id)
    return HttpResponse(document.body)
