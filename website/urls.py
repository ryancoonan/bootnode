# external
from django.conf.urls import url
# internal
import docs
import views

urlpatterns = [
    url(r'^$', views.home),
    url(r'^build/kit/(?P<kit_postfix>.+)$', views.kit),
    url(r'^welcome/', views.welcome),
    url(r'^register/', views.register),
    url(r'^console/', views.console),
    url(r'^validate/(?P<code>[A-Za-z]+)$', views.validate),
    url(r'^reset_password/', views.reset_password),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.reset_password_confirm, name='password_reset_confirm'),
    url(r'^show/(?P<doc_id>[0-9]+)$', docs.show)
]
