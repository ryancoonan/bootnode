# region Imports
from __future__ import absolute_import
# django
from django.apps import apps
from django.contrib import admin
# endregion

app = apps.get_app_config('website')

for model in app.models.itervalues():
    admin.site.register(model)
