DROP DATABASE core;
CREATE DATABASE core CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE core;

CREATE TABLE celery_tasksetmeta (
    id INTEGER NOT NULL AUTO_INCREMENT,
    taskset_id VARCHAR(190),
    result BLOB,
    date_done DATETIME,
    PRIMARY KEY (id),
    UNIQUE (taskset_id)
);

create table celery_taskmeta (
    id integer not null auto_increment,
    task_id varchar(190),
    status varchar(50),
    result blob,
    date_done datetime,
    traceback text,
    primary key (id),
    unique (task_id)
);
