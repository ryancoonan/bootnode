# region Imports
from __future__ import absolute_import
# standard lib
from datetime import timedelta
from django.utils import timezone
import os.path
# django
from django.conf import settings
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
# bootnode
from core.domain.types import ImageCategory
import metadata.models as models
import website.models as web_models
from website.repository.toasts import ToastType
# endregion


class Command(BaseCommand):
    help = 'Populates the database with a base set of test data'

    def handle(self, *args, **options):
        self.stdout.write('Generating test data')
        self.__create_users()
        self.__create_keywords()
        self.__create_documents()
        self.__create_applications()
        self.__create_images()
        self.__create_hosts()
        self.__create_security_flavors()
        self.__create_node_configurations()
        self.__create_kit_configurations()
        self.__create_toasts()
        self.stdout.write('Test data generation complete')

    def __create_applications(self):
        self.stdout.write('\tCreating applications...')
        self.__applications = {}
        self.stdout.write('\t\tCreating application "Apache"...')
        application = models.Application.objects.create(target='http://{public_ipv4}:80/',
                                                        cta='Click here to launch web server', name='Apache')
        application.save()
        self.__applications['apache'] = application

    def __create_document(self, name, title, keywords):
        self.stdout.write('\t\tCreating document "{0}"...'.format(title))
        documents_path = os.path.join(settings.BASE_DIR, 'setup/documents/{0}.html'.format(name))
        with open(documents_path, 'r') as doc_file:
            doc_body = doc_file.read()
        document = models.Document.objects.create(user=self.__users['test@bootnode.com'][0], title=title,
                                                  body=doc_body, pub_date=timezone.now())
        document.save()
        for keyword in keywords:
            document.keywords.add(self.__keywords[keyword])
        document.save()
        self.__documents[name] = document

    def __create_documents(self):
        self.stdout.write('\tCreating documents...')
        self.__documents = {}
        self.__create_document('apache-on-centos', 'Apache on CentOS 7', ['apache', 'centos'])
        self.__create_document('default', 'bootnode default', ['cloud'])
        self.__create_document('rstudio', 'RStudio', ['rstudio', 'r'])

    def __create_host(self, name, region, datacenter):
        self.stdout.write('\t\tCreating host "{0}"...'.format(name))
        host = models.Host.objects.create(name=name, region=region, datacenter=datacenter)
        host.save()
        self.__hosts.append(host)

    def __create_hosts(self):
        self.stdout.write('\tCreating hosts...')
        self.__hosts = []
        self.__create_host('cloud2', 'us-west', 'fremont-he')
        self.__create_host('cloud2', 'us-west', 'dallas-rs')

    def __create_images(self):
        self.stdout.write('\tCreating images...')
        self.__images = {}

        self.stdout.write('\t\tCreating image "Fedora 22"...')
        image = models.Image.objects.create(display_name='Fedora 22', name='bn_fedora22', distro='fedora', version='22',
                                            doc_page=self.__documents['default'], icon='bn_fedora.png',
                                            category=int(ImageCategory.os), application=self.__applications['apache'])
        image.save()
        image.keywords.add(self.__keywords['apache'])
        image.save()
        self.__images['fedora22'] = image

    def __create_keywords(self):
        self.stdout.write('\tCreating keywords')
        self.__keywords = {}
        for word in ['apache', 'centos', 'cloud', 'docker', 'rstudio', 'r']:
            keyword = models.Keyword.objects.create(
                keyword_text=word
            )
            keyword.save()
            self.__keywords[word] = keyword
        self.stdout.write('\t\tCreated {0} keywords'.format(len(self.__keywords)))

    def __create_kit_configuration(self, default_node_name, name, landing_layout, link_postfix, application_name,
                                   node_configuration_names):
        self.stdout.write('\t\tCreating kit configuration "{0}"...'.format(name))
        application = self.__applications[application_name] if application_name is not None else None
        kit_configuration = models.KitConfiguration.objects.create(
            default_node=self.__node_configurations[default_node_name], name=name, landing_layout=landing_layout,
            link_postfix=link_postfix, application=application)
        kit_configuration.save()
        for node_configuration_name in node_configuration_names:
            kit_configuration.node_configurations.add(self.__node_configurations[node_configuration_name])
        kit_configuration.save()

    def __create_kit_configurations(self):
        self.stdout.write('\tCreating kit configurations...')
        self.__kit_configurations = {}
        self.__create_kit_configuration('ApacheServer', 'Web Server', '', 'webserver', 'apache', ['ApacheServer'])
        self.__create_kit_configuration('ApacheServer', 'Web Stack', '', 'webstack', 'apache',
                                        ['ApacheServer', 'DatabaseServer'])

    def __create_node_configuration(self, cpus, disk, memory, image_name, name, security_flavor_names, ip=False):
        self.stdout.write('\tCreating node configuration "{0}"...'.format(name))
        flavor = models.Flavor.get_or_create_from_configuration(cpus, disk, memory)
        node_configuration = models.NodeConfiguration.objects.create(flavor=flavor, image=self.__images[image_name],
                                                                     name=name, has_public_ip=ip)
        node_configuration.save()
        for security_flavor_name in security_flavor_names:
            node_configuration.security_flavors.add(self.__security_flavors[security_flavor_name])
        node_configuration.save()
        self.__node_configurations[name] = node_configuration

    def __create_node_configurations(self):
        self.stdout.write('\tCreating node configurations...')
        self.__node_configurations = {}
        self.__create_node_configuration(1, 5, 1024, 'fedora22', 'ApacheServer', ['default'], ip=True)
        self.__create_node_configuration(1, 10, 1024, 'fedora22', 'DatabaseServer', ['default', 'extra'])

    def __create_security_flavor(self, name, rules):
        self.stdout.write('\t\tCreating security flavor "{0}"...'.format(name))
        security_flavor = models.SecurityFlavor.objects.create(name=name)
        security_flavor.save()
        self.__create_security_rules(security_flavor, rules)
        self.__security_flavors[name] = security_flavor

    def __create_security_flavors(self):
        self.stdout.write('\tCreating security flavors...')
        self.__security_flavors = {}
        self.__create_security_flavor('default', [
            {'port': '80', 'direction': 'ingress', 'protocol': 'tcp', 'ethertype': 'IPv4'},
            {'port': '22', 'direction': 'ingress', 'protocol': 'tcp', 'ethertype': 'IPv4'},
            {'port': '80', 'direction': 'egress', 'protocol': 'tcp', 'ethertype': 'IPv4'},
            {'port': '22', 'direction': 'egress', 'protocol': 'tcp', 'ethertype': 'IPv4'},
        ])
        self.__create_security_flavor('extra', [
            {'port': '9090', 'direction': 'egress', 'protocol': 'tcp', 'ethertype': 'IPv4'},
            {'port': '9090', 'direction': 'ingress', 'protocol': 'tcp', 'ethertype': 'IPv4'},
            {'port': '4200', 'direction': 'egress', 'protocol': 'tcp', 'ethertype': 'IPv4'},
            {'port': '4200', 'direction': 'ingress', 'protocol': 'tcp', 'ethertype': 'IPv4'}
        ])

    def __create_security_rules(self, security_flavor, rules):
        self.stdout.write('\t\t\tCreating security rules...')
        for rule in rules:
            security_rule = models.SecurityRule.objects.create(port=rule['port'], direction=rule['direction'],
                                                               protocol=rule['protocol'], ethertype=rule['ethertype'])
            security_rule.save()
            security_flavor.rules.add(security_rule)
        security_flavor.save()

    def __create_toast(self, text, priority, expires, type_=ToastType.info, name=None):
        self.stdout.write('\t\tCreating toast "{0}"...'.format(text))
        toast = web_models.Toast.objects.create(text=text, priority=priority, expires=expires, type=type_, name=name)
        toast.save()
        self.__toasts.append(toast)

    def __create_toasts(self):
        self.stdout.write('\tCreating toasts...')
        self.__toasts = []
        self.__create_toast('Welcome to the bootnode beta! Send bugs or feedback to beta@bootnode.com', 100,
                            timezone.now() + timedelta(days=90), name='welcome')
        self.__create_toast('This is another toast, maybe it says something', 10,
                            timezone.now() + timedelta(days=15))
        self.__create_toast('Please confirm your email to keep your account active. You have {time_left}', 1000,
                            None, ToastType.lockout_timer)
        self.__create_toast('Thanks for confirming your email!', 800,
                            None, ToastType.info_show_once, 'email-validated')
        self.__create_toast('Your account has been locked - verify your email address to continue', 1000,
                            None, ToastType.locked_out, 'account-locked')

    def __create_users(self):
        self.stdout.write('\tCreating users...')
        self.stdout.write('\t\tCreating auth user...')
        user = User.objects.create_user('test@bootnode.com', 'test@bootnode.com', 'testtest')
        user.is_superuser = True
        user.is_staff = True
        user.save()

        self.stdout.write('\t\tCreating user detail...')
        user_detail = models.UserDetail.objects.create(user=user, console_password='bootnode!', allowed_nodes=3,
                                                       allowed_ips=1)
        user_detail.save()

        self.stdout.write('\t\tCreating ssh key...')
        ssh_key = models.SshKey.create(user)
        ssh_key.save()
        self.__users = {
            'test@bootnode.com': (user, user_detail, ssh_key)
        }
